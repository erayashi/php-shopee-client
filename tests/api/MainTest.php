<?php
/**
 * Created by eaz.
 * Date: 16/02/19
 * Time: 14.41
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrapTest\api;


use ErryAz\ShopeeWrap\Shopee;
use ErryAz\ShopeeWrapTest\BaseTestCase;
use ErryAz\ShopeeWrapTest\ShopeeTestTrait;

class MainTest extends BaseTestCase
{
    use ShopeeTestTrait;

    /** @var Shopee */
    private static $shopee;

    public static function setUpBeforeClass()
    {
        self::createClient();
        self::$shopee = self::$client->shopee;
    }

    public static function testRedirectUrl() {
        $url = self::$shopee->redirect('http://139.59.124.159/shopee/sandbox');
        echo $url;
        static::assertTrue(is_string($url));
    }
}
