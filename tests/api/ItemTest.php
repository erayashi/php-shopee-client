<?php
/**
 * Created by eaz.
 * Date: 31/10/18
 * Time: 9:31
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrapTest\api;


use ErryAz\ShopeeWrap\api\ItemApi;
use ErryAz\ShopeeWrap\handler\exception\ShopeeResException;
use ErryAz\ShopeeWrap\models\item\request\AddItemRequest;
use ErryAz\ShopeeWrap\models\item\request\AttributesRequest;
use ErryAz\ShopeeWrap\models\item\request\CategoriesCountryRequest;
use ErryAz\ShopeeWrap\models\item\request\ItemDetailRequest;
use ErryAz\ShopeeWrap\models\item\request\ItemListRequest;
use ErryAz\ShopeeWrapTest\BaseTestCase;
use ErryAz\ShopeeWrapTest\ShopeeTestTrait;
use Exception;
use PHPUnit\Framework\TestCase;

class ItemTest extends BaseTestCase
{
    use ShopeeTestTrait;
    /** @var ItemApi */
    private static $itemApi;

    public static function setUpBeforeClass()
    {
        self::createClient();
        self::$itemApi = self::$client->item();
    }

    public function testGetCategories(){
        try {
            $request = new CategoriesCountryRequest();
            $request->setCountry("ID");
            $request->setIsCb(false);
            $request->toRequestData();
            print_r($request);
            $getCategories = self::$itemApi->getCategoriesByCountry($request);
            print_r($getCategories);
            $this->assertTrue(count($getCategories->categories) > 0);
        } catch (Exception $e) {
            dump($e);
        }
    }

    public function testGetAttributes(){
        try {
            $request = new AttributesRequest();
            $request->setCategoryId(14224);
            $request->toRequestData();
            print_r($request);
            $getAttributes = self::$itemApi->getAttributes($request);
            print_r($getAttributes);
            $this->assertTrue(count($getAttributes->attributes) > 0);
        } catch (ShopeeResException $e) {
            echo $e->getMessage();
        }
    }

    public function testGetItems(){
        try {
            $request = new ItemListRequest([
                'pagination_offset' => 0,
                'pagination_entries_per_page' => 50
            ]);
            $getItem = self::$client->item()->getList($request);
            dump($getItem);
            $this->assertTrue(count($getItem->items) <= 50);
        } catch (ShopeeResException $e) {
            echo $e->getMessage();
        }
    }

    public function testAddItem(){
        $json = '{
                "category_id" : "14224",
                "name" : "17",
                "description" : "17poqwepqiwpejkqpwodjpqnwsdpansdpkqmsndpqwsnpdlanspdkakshdpasjdpaosdkjjpqosjdpqsmdmpqsjdpqsmmdpoqjmspdoqwsmpd",
                "price" : "12000.0",
                "stock" : "17",
                "logistics" : [
                    {
                        "logistic_id" : "80011",
                        "enabled" : true
                    }
                ],
                "weight" : "17",
                "images" : [
                    {
                        "url" : "https://ecs7.tokopedia.net/img/cache/700/product-1/2016/10/13/1357109/1357109_af7b5776-7341-499d-8006-119247b54675.jpg"
                    }
                ],
                "attributes" : [
                    {
                        "attributes_id" : 1445,
                        "value" : "ABC"
                    }
                ]
            }';
        try {
            $request = new AddItemRequest($json);
//            var_dump($request);
            $addItem = self::$itemApi->add($request);
            dump($addItem);
            $this->assertTrue(isset($addItem->item));
        } catch (ShopeeResException $e){
            echo $e->getMessage();
        } catch (Exception $e) {
            echo $e->getTraceAsString();
        }
    }

    public function testGetUrl(){
        $url_login = self::$client->shopee->redirect("http://139.59.124.159/shopee/sandbox");
        echo $url_login;
        $this->assertTrue(is_string($url_login));
    }

    public function testGetDetail() {
        $req = new ItemDetailRequest(['item_id' => 1516291]);
        $this->runOnCatch(function () use ($req) {
            $getDetail = self::$itemApi->getDetail($req);
            dump($getDetail);
            $this->assertTrue(isset($getDetail));
        });
    }
}
