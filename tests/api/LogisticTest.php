<?php
/**
 * Created by eaz.
 * Date: 21/12/18
 * Time: 16:42
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrapTest\api;


use ErryAz\ShopeeWrap\api\LogisticApi;
use ErryAz\ShopeeWrap\models\BaseRequest;
use ErryAz\ShopeeWrap\models\logistic\request\ParameterForInitRequest;
use ErryAz\ShopeeWrap\models\logistic\request\TimeSlotRequest;
use ErryAz\ShopeeWrapTest\BaseTestCase;
use ErryAz\ShopeeWrapTest\ShopeeTestTrait;

class LogisticTest extends BaseTestCase
{
    use ShopeeTestTrait;
    /** @var LogisticApi */
    private static $logisticApi;

    public static function setUpBeforeClass()
    {
        self::createClient();
        self::$logisticApi = self::$client->logistic();
    }

    public function testGetParameterLogistic(){
        $request = new ParameterForInitRequest();
        $request->setOrdersn("181218144708TU0");
        $this->runOnCatch(function () use ($request){
            $paramLogistic = self::$logisticApi->getInitParameter($request->toRequestData());
            dump($paramLogistic);
            $this->assertTrue(isset($paramLogistic));
        });
    }

    public function testGetLogistics() {
        $request = new BaseRequest();
        $this->runOnCatch(function () use ($request){
            $logistics = self::$logisticApi->getList($request);
            dump($logistics);
            $this->assertTrue(is_array($logistics->logistics));
        });
    }

    public function testGetAddress(){
        $request = new BaseRequest();
        $this->runOnCatch(function () use ($request){
            $address = self::$logisticApi->getAddress($request->toRequestData());
            dump($address);
            $this->assertTrue(is_array($address->address_list));
        });
    }

    public function testGetTimeSlot(){
        $request = new TimeSlotRequest();
        $request->setOrdersn('181218144708TU0');
        $request->setAddressId(817);
        $this->runOnCatch(function () use ($request){
            $timeSlot = self::$logisticApi->getTimeSlot($request);
            dump($timeSlot);
            $this->assertTrue(is_array($timeSlot->pickup_time));
        });
    }
}
