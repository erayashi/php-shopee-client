<?php
/**
 * Created by eaz.
 * Date: 21/12/18
 * Time: 15:26
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrapTest\api;


use ErryAz\ShopeeWrap\api\OrderApi;
use ErryAz\ShopeeWrap\handler\exception\ShopeeResException;
use ErryAz\ShopeeWrap\models\order\request\OrderCancelRequest;
use ErryAz\ShopeeWrap\models\order\request\OrderDetailsRequest;
use ErryAz\ShopeeWrap\models\order\request\OrderListByStatusRequest;
use ErryAz\ShopeeWrapTest\BaseTestCase;
use ErryAz\ShopeeWrapTest\ShopeeTestTrait;

class OrderTest extends BaseTestCase
{
    use ShopeeTestTrait;
    /** @var OrderApi */
    private static $orderApi;
    public static function setUpBeforeClass()
    {
        self::createClient();
        self::$orderApi = self::$client->order();
    }

    public function testGetOrders() {
        $request = new OrderListByStatusRequest();
        $request->setOrderStatus("UNPAID");

        try {
            $orders = self::$orderApi->getByStatus($request->toRequestData());
            dump($orders);
            $this->assertTrue(is_array($orders->orders));
        } catch (ShopeeResException $e) {
            dd($e->getMessage());
        }
    }

    public function testGetDetail() {
        $request = new OrderDetailsRequest(['ordersn_list' =>
            ['18100513549H1Q0']]
        );
        $this->runOnCatch(function () use ($request) {
            $orders = self::$orderApi->getDetails($request);
            print_r($orders);
            $this->assertTrue(is_array($orders->orders));
        });
    }

    public function testCancel() {
        $request = new OrderCancelRequest([
            'ordersn'       => '1902121710095D9',
            'cancel_reason' => 'OUT_OF_STOCK',
            'item_id'       => '1516291',
            'variation_id'  => '1363060'
        ]);

        $this->runOnCatch(function () use ($request) {
            $cancelOrder = self::$orderApi->cancel($request);
            print_r($cancelOrder);
            $this->assertTrue(isset($cancelOrder->modified_time));
        });
    }
}
