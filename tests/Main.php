<?php
/**
 * Created by eaz.
 * Date: 30/10/18
 * Time: 18:39
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrapTest;


use ErryAz\ShopeeWrap\Clients;

class Main extends Clients
{
    public function __construct()
    {
        //using sandbox data
//        parent::__construct([
//            'partner_id'    => 100166,
//            'partner_key'   => "f033b829f25af1c852eeb8507ee0f124cab2bd5a93ba85ba35e17bb6d7f1cd25",
//            'production'    => false,
//            'shop_id'       => 211541
////            'shop_id'       => 204240
//        ]);

// live
        parent::__construct([
            'partner_id'    => 840468,
            'partner_key'   => "1f8699875251e38ca3f9e1da5666871198d8904e05dd161422cb70aa7c7bb10d",
            'production'    => true,
            'shop_id'       => 33342929
        ]);
    }
}
