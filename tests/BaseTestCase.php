<?php
/**
 * Created by eaz.
 * Date: 21/12/18
 * Time: 16:46
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrapTest;


use PHPUnit\Framework\TestCase;

class BaseTestCase extends TestCase
{
    public function runOnCatch(callable $run, callable $onError = null){
        try {
            $run();
        } catch (\Exception $e){
            if(!is_null($onError)) $onError();
            dump($e->getMessage());
            echo $e->getTraceAsString();
            $this->assertTrue(false);
        }
    }
}
