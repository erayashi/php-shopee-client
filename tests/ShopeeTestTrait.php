<?php
/**
 * Created by eaz.
 * Date: 02/11/18
 * Time: 17:25
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrapTest;


trait ShopeeTestTrait
{
    /** @var Main */
    private static $client;

    public static function createClient()
    {
        self::$client = new Main();
    }
}
