<?php
/**
 * Created by eaz.
 * Date: 30/10/18
 * Time: 9:53
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models;


class BaseRequest extends BaseFormater
{
    /** @var integer */
    public $partner_id;
    /** @var integer */
    public $shopid;
    /** @var integer */
    public $timestamp;

    /**
     * @return int
     */
    public function getPartnerId(): int
    {
        return $this->partner_id;
    }

    /**
     * @param int $partner_id
     */
    public function setPartnerId(int $partner_id)
    {
        $this->partner_id = $partner_id;
    }

    /**
     * @return int
     */
    public function getShopid(): int
    {
        return $this->shopid;
    }

    /**
     * @param int $shopid
     */
    public function setShopid(int $shopid)
    {
        $this->shopid = $shopid;
    }

    /**
     * @return int
     */
    public function getTimestamp(): int
    {
        return $this->timestamp;
    }

    /**
     * @param int $timestamp
     */
    public function setTimestamp(int $timestamp)
    {
        $this->timestamp = $timestamp;
    }
}
