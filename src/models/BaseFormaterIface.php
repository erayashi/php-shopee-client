<?php
/**
 * Created by eaz.
 * Date: 03/11/18
 * Time: 7:05
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models;


interface BaseFormaterIface
{
    public function fromArray($reqParam);
    public function toRequestData();
}
