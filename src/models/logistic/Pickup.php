<?php
/**
 * Created by eaz.
 * Date: 21/12/18
 * Time: 14:09
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\logistic;


use ErryAz\ShopeeWrap\models\BaseFormater;

class Pickup extends BaseFormater
{
    use TrackingNo;
    /** @var int */
    public $address_id;
    /** @var string */
    public $pickup_time_id;

    /**
     * @param int $address_id
     */
    public function setAddressId(int $address_id)
    {
        $this->address_id = $address_id;
    }

    /**
     * @param string $pickup_time_id
     */
    public function setPickupTimeId(string $pickup_time_id)
    {
        $this->pickup_time_id = $pickup_time_id;
    }
}
