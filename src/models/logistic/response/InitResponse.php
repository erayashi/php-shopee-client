<?php
/**
 * Created by eaz.
 * Date: 24/02/19
 * Time: 21.32
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\logistic\response;


use ErryAz\ShopeeWrap\models\BaseResponse;

class InitResponse extends BaseResponse
{
    /** @var string */
    public $tracking_number;

    /**
     * @param string $tracking_number
     */
    public function setTrackingNumber(string $tracking_number)
    {
        $this->tracking_number = $tracking_number;
    }
}
