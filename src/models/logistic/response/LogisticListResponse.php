<?php
/**
 * Created by eaz.
 * Date: 06/11/18
 * Time: 9:29
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\logistic\response;


use ErryAz\ShopeeWrap\models\BaseResponse;
use ErryAz\ShopeeWrap\models\item\Logistics;

class LogisticListResponse extends BaseResponse
{
    /** @var Logistics */
    public $logistics;

    /**
     * @param Logistics $logistics
     */
    public function setLogistics(Logistics $logistics)
    {
        $this->logistics = $logistics;
    }
}
