<?php
/**
 * Created by eaz.
 * Date: 21/12/18
 * Time: 14:11
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\logistic;


use ErryAz\ShopeeWrap\models\BaseFormater;

class Dropoff extends BaseFormater
{
    use TrackingNo;
    /** @var int */
    public $branch_id;
    /** @var string */
    public $sender_real_name;


    /**
     * @param int $branch_id
     */
    public function setBranchId(int $branch_id)
    {
        $this->branch_id = $branch_id;
    }

    /**
     * @param string $sender_real_name
     */
    public function setSenderRealName(string $sender_real_name)
    {
        $this->sender_real_name = $sender_real_name;
    }
}
