<?php
/**
 * Created by eaz.
 * Date: 21/12/18
 * Time: 17:09
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\logistic\request;


use ErryAz\ShopeeWrap\models\BaseRequest;
use ErryAz\ShopeeWrap\models\order\OrderSnTrait;

class TimeSlotRequest extends BaseRequest
{
    use OrderSnTrait;
    /** @var int */
    public $address_id;

    /**
     * @param int $address_id
     */
    public function setAddressId(int $address_id)
    {
        $this->address_id = $address_id;
    }
}
