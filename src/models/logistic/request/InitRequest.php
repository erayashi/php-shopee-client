<?php
/**
 * Created by eaz.
 * Date: 06/11/18
 * Time: 16:09
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\logistic\request;


use ErryAz\ShopeeWrap\models\BaseRequest;
use ErryAz\ShopeeWrap\models\logistic\Dropoff;
use ErryAz\ShopeeWrap\models\logistic\NonIntegrated;
use ErryAz\ShopeeWrap\models\logistic\Pickup;
use ErryAz\ShopeeWrap\models\order\OrderSnTrait;

class InitRequest extends BaseRequest
{
    use OrderSnTrait;
    /** @var Pickup */
    public $pickup;
    /** @var Dropoff */
    public $dropoff;
    /** @var NonIntegrated */
    public $non_integrated;

    /**
     * @param Pickup $pickup
     */
    public function setPickup(Pickup $pickup)
    {
        $this->pickup = $pickup;
    }

    /**
     * @param Dropoff $dropoff
     */
    public function setDropoff(Dropoff $dropoff)
    {
        $this->dropoff = $dropoff;
    }

    /**
     * @param NonIntegrated $non_integrated
     */
    public function setNonIntegrated(NonIntegrated $non_integrated)
    {
        $this->non_integrated = $non_integrated;
    }
}
