<?php
/**
 * Created by eaz.
 * Date: 21/12/18
 * Time: 15:39
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\logistic\request;


use ErryAz\ShopeeWrap\models\BaseRequest;
use ErryAz\ShopeeWrap\models\order\OrderSnTrait;

class ParameterForInitRequest extends BaseRequest
{
    use OrderSnTrait;
}
