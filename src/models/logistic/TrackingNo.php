<?php
/**
 * Created by eaz.
 * Date: 21/12/18
 * Time: 14:12
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\logistic;


trait TrackingNo
{
    /** @var string */
    public $tracking_no;

    /**
     * @param string $tracking_no
     */
    public function setTrackingNo(string $tracking_no)
    {
        $this->tracking_no = $tracking_no;
    }


}
