<?php
/**
 * Created by eaz.
 * Date: 21/12/18
 * Time: 14:14
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\logistic;


use ErryAz\ShopeeWrap\models\BaseFormater;

class NonIntegrated extends BaseFormater
{
    use TrackingNo;
}
