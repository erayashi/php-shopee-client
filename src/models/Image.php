<?php

/*
 * This file is part of <package name>.
 *
 * This source file is subject to the license that is bundled
 * with this source code in the file LICENSE.
 */

namespace ErryAz\ShopeeWrap\models;

class Image extends BaseRequest
{
    /** @var string */
    public $url;

    public function __construct($url = null)
    {
        if(is_string($url)) $this->setUrl($url);
        else parent::__construct($url);
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url)
    {
        $this->url = $url;
    }
}
