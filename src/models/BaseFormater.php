<?php

/*
 * This file is part of <package name>.
 *
 * This source file is subject to the license that is bundled
 * with this source code in the file LICENSE.
 */

namespace ErryAz\ShopeeWrap\models;

use ReflectionClass;

class BaseFormater implements BaseFormaterIface
{
    const mandatory_fields = [];
    /**
     * BaseFormater constructor.
     *
     * @param null $requestData
     */
    public function __construct($requestData = null)
    {
        if (null !== $requestData) {
            $this->fromArray($requestData);
        }
    }

    /**
     * @param \stdClass|array $reqParams
     *
     * @return BaseFormaterIface
     */
    public function fromArray($reqParams)
    {
        if (is_string($reqParams)) {
            $reqParams = json_decode($reqParams, true);
        }

        try {
            $reflection = new ReflectionClass($this);
        } catch (\ReflectionException $e) {
            return null;
        }

        $reqParamsArray = (array) $reqParams;
//        $this->objToArray($reqParams, $reqParamsArray);

        foreach ($reqParamsArray as $param => $value) {
            $setter = 'set'.$this->toCamelcase($param, true);
            if (method_exists($this, $setter)) {
                $reflectionMethod = $reflection->getMethod($setter);
                $parameterType = $reflectionMethod->getParameters()[0]->getType();
                if ($parameterType && !$parameterType->isBuiltin()) {
                    $className = $parameterType->getName();
                    $value = new $className($value);
                }
                $this->$setter($value);
                continue;
            }
            if(property_exists($this, $param)) $this->$param = $value;
        }

        return $this->toRequestData();
    }

    /**
     * @param string $name
     * @param bool $lowCharFirst
     *
     * @return string
     */
    protected function toCamelcase(string $name, bool $lowCharFirst = false): string
    {
        $name = str_replace('_', '', ucwords($name, '_'));
        if ($lowCharFirst) {
            $name = lcfirst($name);
        }

        return $name;
    }

    protected function objToArray($obj, &$arr)
    {
        if (!is_object($obj) && !is_array($obj)) {
            $arr = $obj;

            return $arr;
        }

        foreach ($obj as $key => $value) {
            if (!empty($value)) {
                $arr[$key] = [];
                $this->objToArray($value, $arr[$key]);
            } else {
                $arr[$key] = $value;
            }
        }

        return $arr;
    }

    /**
     * @return $this
     */
    public function toRequestData()
    {
        foreach ($this as $key => $value) {
            if (null === $value && !in_array($key, static::mandatory_fields)) {
                unset($this->$key);
            } elseif (is_string($value) && empty(trim($value)) && !in_array($key, static::mandatory_fields)) {
                unset($this->$key);
            } elseif ($value instanceof BaseFormaterIface || $value instanceof BaseFormaterCollectionIface) {
                $this->$key = $value->toRequestData();
            }
        }

        return $this;
    }
}
