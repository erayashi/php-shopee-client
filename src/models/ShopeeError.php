<?php
/**
 * Created by eaz.
 * Date: 31/10/18
 * Time: 11:49
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models;


class ShopeeError
{
    private $msg;
    private $error;

    /**
     * @return mixed
     */
    public function getMsg()
    {
        return $this->msg;
    }

    /**
     * @param mixed $msg
     */
    public function setMsg($msg)
    {
        $this->msg = $msg;
    }

    /**
     * @return mixed
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @param mixed $error
     */
    public function setError($error)
    {
        $this->error = $error;
    }
}
