<?php
/**
 * Created by eaz.
 * Date: 05/02/19
 * Time: 21:09
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\order;


use ErryAz\ShopeeWrap\models\BaseFormaterCollection;

class Orders extends BaseFormaterCollection
{

    public function fromArray(array $reqParam)
    {
        foreach ($reqParam as $param){
            $this->add(new Order($param));
        }

        return $this->array_data;
    }
}
