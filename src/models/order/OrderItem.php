<?php
/**
 * Created by eaz.
 * Date: 06/11/18
 * Time: 15:39
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\order;


use ErryAz\ShopeeWrap\models\BaseFormater;
use ErryAz\ShopeeWrap\models\item\ItemIdTrait;
use ErryAz\ShopeeWrap\models\item\VariationIdTrait;

class OrderItem extends BaseFormater
{
    use ItemIdTrait;
    use VariationIdTrait;
    /** @var string */
    public $item_name;
    /** @var string */
    public $item_sku;
    /** @var string */
    public $variation_name;
    /** @var string */
    public $variation_sku;
    /** @var int */
    public $variation_quantity_purchased;
    /** @var float */
    public $variation_original_price;
    /** @var float */
    public $variation_discounted_price;
    /** @var int */
    public $is_wholesale;
}
