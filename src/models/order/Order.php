<?php
/**
 * Created by eaz.
 * Date: 06/11/18
 * Time: 15:27
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\order;


use ErryAz\ShopeeWrap\models\BaseFormater;

class Order extends BaseFormater
{
    use OrderSnTrait;

    /** @var string */
    public $country;
    /** @var string */
    public $currency;
    /** @var int */
    public $cod;
    /** @var string */
    public $tracking_no;
    /** @var int */
    public $days_to_ship;
    /** @var RecipientAddress */
    public $recipient_address;
    /** @var float */
    public $estimated_shipping_fee;
    /** @var float */
    public $actual_shipping_cost;
    /** @var float */
    public $total_amount;
    /** @var float */
    public $escrow_amount;
    /** @var string */
    public $order_status;
    /** @var string */
    public $shipping_carrier;
    /** @var string */
    public $payment_method;
    /** @var bool */
    public $goods_to_declare;
    /** @var string */
    public $message_to_seller;
    /** @var string */
    public $note;
    /** @var string */
    public $note_update_time;
    /** @var int */
    public $create_time;
    /** @var int */
    public $update_time;
    /** @var OrderItems */
    public $items;
    /** @var int */
    public $pay_time;
    /** @var string */
    public $dropshipper;
}
