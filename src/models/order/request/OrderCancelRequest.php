<?php
/**
 * Created by eaz.
 * Date: 16/02/19
 * Time: 12.27
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\order\request;


use ErryAz\ShopeeWrap\models\BaseRequest;
use ErryAz\ShopeeWrap\models\item\ItemIdTrait;
use ErryAz\ShopeeWrap\models\item\VariationIdTrait;
use ErryAz\ShopeeWrap\models\order\OrderSnTrait;

class OrderCancelRequest extends BaseRequest
{
    const REASON_OUT_OF_STOCK       = 'OUT_OF_STOCK';
    const REASON_CUSTOMER_REQUEST   = 'CUSTOMER_REQUEST';
    const REASON_UNDELIVERABLE_AREA = 'UNDELIVERABLE_AREA';
    const REASON_COD_NOT_SUPPORTED  = 'COD_NOT_SUPPORTED';

    use OrderSnTrait, ItemIdTrait, VariationIdTrait;

    /** @var string */
    public $cancel_reason;

    /**
     * @param string $cancel_reason
     */
    public function setCancelReason(string $cancel_reason)
    {
        $this->cancel_reason = $cancel_reason;
    }
}
