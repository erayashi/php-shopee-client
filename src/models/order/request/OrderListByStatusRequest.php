<?php
/**
 * Created by eaz.
 * Date: 06/11/18
 * Time: 14:29
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\order\request;


class OrderListByStatusRequest extends OrderListRequest
{
    /** @var string */
    public $order_status;

    /**
     * @return string
     */
    public function getOrderStatus(): string
    {
        return $this->order_status;
    }

    /**
     * @param string $order_status
     */
    public function setOrderStatus(string $order_status)
    {
        $this->order_status = $order_status;
    }


}
