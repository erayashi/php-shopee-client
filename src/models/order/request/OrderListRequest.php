<?php
/**
 * Created by eaz.
 * Date: 02/11/18
 * Time: 18:57
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\order\request;

use ErryAz\ShopeeWrap\models\BaseRequest;
use ErryAz\ShopeeWrap\models\PagingTrait;

class OrderListRequest extends BaseRequest
{
    use PagingTrait;

    /** @var int */
    public $create_time_from;
    /** @var int */
    public $create_time_to;

    /**
     * @return int
     */
    public function getCreateTimeFrom() : int
    {
        return $this->create_time_from;
    }

    /**
     * @param mixed $create_time_from
     */
    public function setCreateTimeFrom(int $create_time_from)
    {
        $this->create_time_from = $create_time_from;
    }

    /**
     * @return int
     */
    public function getCreateTimeTo() : int
    {
        return $this->create_time_to;
    }

    /**
     * @param mixed $create_time_to
     */
    public function setCreateTimeTo(int $create_time_to)
    {
        $this->create_time_to = $create_time_to;
    }
}
