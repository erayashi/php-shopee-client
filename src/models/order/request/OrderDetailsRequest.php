<?php
/**
 * Created by eaz.
 * Date: 06/11/18
 * Time: 14:27
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\order\request;


use ErryAz\ShopeeWrap\models\BaseRequest;

class OrderDetailsRequest extends BaseRequest
{
    /** @var string[] */
    public $ordersn_list;

    /**
     * @return string[]
     */
    public function getOrdersnList(): array
    {
        return $this->ordersn_list;
    }

    /**
     * @param string[] $ordersn_list
     */
    public function setOrdersnList(array $ordersn_list)
    {
        $this->ordersn_list = $ordersn_list;
    }


}
