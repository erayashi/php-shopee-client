<?php
/**
 * Created by eaz.
 * Date: 06/11/18
 * Time: 15:42
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\order;


use ErryAz\ShopeeWrap\models\BaseFormaterCollection;

class OrderItems extends BaseFormaterCollection
{

    /**
     * @param array $reqParam
     * @return array
     * @throws \ReflectionException
     */
    public function fromArray(array $reqParam)
    {
        foreach ($reqParam as $param){
            $this->add(new OrderItem($param));
        }

        return $this->array_data;
    }
}
