<?php
/**
 * Created by eaz.
 * Date: 04/11/18
 * Time: 23:12
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\order;


trait OrderSnTrait
{
    /** @var string */
    public $ordersn;

    /**
     * @return string
     */
    public function getOrdersn(): string
    {
        return $this->ordersn;
    }

    /**
     * @param string $ordersn
     */
    public function setOrdersn(string $ordersn)
    {
        $this->ordersn = $ordersn;
    }
}
