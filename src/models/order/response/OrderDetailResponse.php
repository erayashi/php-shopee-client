<?php
/**
 * Created by eaz.
 * Date: 06/11/18
 * Time: 15:52
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\order\response;


use ErryAz\ShopeeWrap\models\BaseResponse;
use ErryAz\ShopeeWrap\models\order\Order;
use ErryAz\ShopeeWrap\models\order\Orders;

class OrderDetailResponse extends BaseResponse
{
    /** @var Orders */
    public $orders;
    /** @var string[] */
    public $errors;

    /**
     * @param Orders $orders
     */
    public function setOrders(Orders $orders)
    {
        $this->orders = $orders;
    }

    /**
     * @param string[] $errors
     */
    public function setErrors(array $errors)
    {
        $this->errors = $errors;
    }
}
