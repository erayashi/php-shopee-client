<?php
/**
 * Created by eaz.
 * Date: 06/11/18
 * Time: 15:29
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\order;


use ErryAz\ShopeeWrap\models\BaseFormater;

class RecipientAddress extends BaseFormater
{
    /** @var string */
    public $name;
    /** @var string */
    public $phone;
    /** @var string */
    public $town;
    /** @var string */
    public $district;
    /** @var string */
    public $city;
    /** @var string */
    public $state;
    /** @var string */
    public $country;
    /** @var string */
    public $zipcode;
    /** @var string */
    public $full_address;
}
