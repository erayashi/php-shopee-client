<?php
/**
 * Created by eaz.
 * Date: 03/11/18
 * Time: 20:03
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models;


abstract class BaseFormaterCollection implements BaseFormaterCollectionIface
{
    /** @var array */
    public $array_data = [];

    /**
     * BaseFormaterCollection constructor.
     * @param array $array_data
     */
    public function __construct(array $array_data)
    {
        $this->fromArray($array_data);
    }

    public function add(BaseFormaterIface $data){
        $this->array_data[] = $data;
        return $this;
    }

    abstract public function fromArray(array $reqParam);

    public function toRequestData()
    {
        return array_map(function(BaseFormaterIface $data){
            return $data->toRequestData();
        }, $this->array_data);
    }

}
