<?php
/**
 * Created by eaz.
 * Date: 08/11/18
 * Time: 13:06
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\shop\response;


use ErryAz\ShopeeWrap\models\BaseResponse;
use ErryAz\ShopeeWrap\models\shop\ShopTrait;

class ShopInfoResponse extends BaseResponse
{
    use ShopTrait;
}
