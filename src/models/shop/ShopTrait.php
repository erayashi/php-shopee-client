<?php
/**
 * Created by eaz.
 * Date: 08/11/18
 * Time: 13:04
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\shop;


trait ShopTrait
{
    /** @var int */
    public $shop_id;
    /** @var string */
    public $shop_name;
    /** @var string */
    public $country;
    /** @var string */
    public $shop_description;
    /** @var array */
    public $videos;
    /** @var array */
    public $images;
    /** @var int */
    public $disable_make_offer;
    /** @var int */
    public $enable_display_unitno;
    /** @var int */
    public $item_limit;

    /**
     * @return int
     */
    public function getShopId(): int
    {
        return $this->shop_id;
    }

    /**
     * @param int $shop_id
     */
    public function setShopId(int $shop_id)
    {
        $this->shop_id = $shop_id;
    }

    /**
     * @return string
     */
    public function getShopName(): string
    {
        return $this->shop_name;
    }

    /**
     * @param string $shop_name
     */
    public function setShopName(string $shop_name)
    {
        $this->shop_name = $shop_name;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry(string $country)
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getShopDescription(): string
    {
        return $this->shop_description;
    }

    /**
     * @param string $shop_description
     */
    public function setShopDescription(string $shop_description)
    {
        $this->shop_description = $shop_description;
    }

    /**
     * @return array
     */
    public function getVideos(): array
    {
        return $this->videos;
    }

    /**
     * @param array $videos
     */
    public function setVideos(array $videos)
    {
        $this->videos = $videos;
    }

    /**
     * @return array
     */
    public function getImages(): array
    {
        return $this->images;
    }

    /**
     * @param array $images
     */
    public function setImages(array $images)
    {
        $this->images = $images;
    }

    /**
     * @return int
     */
    public function getDisableMakeOffer(): int
    {
        return $this->disable_make_offer;
    }

    /**
     * @param int $disable_make_offer
     */
    public function setDisableMakeOffer(int $disable_make_offer)
    {
        $this->disable_make_offer = $disable_make_offer;
    }

    /**
     * @return int
     */
    public function getEnableDisplayUnitno(): int
    {
        return $this->enable_display_unitno;
    }

    /**
     * @param int $enable_display_unitno
     */
    public function setEnableDisplayUnitno(int $enable_display_unitno)
    {
        $this->enable_display_unitno = $enable_display_unitno;
    }

    /**
     * @return int
     */
    public function getItemLimit(): int
    {
        return $this->item_limit;
    }

    /**
     * @param int $item_limit
     */
    public function setItemLimit(int $item_limit)
    {
        $this->item_limit = $item_limit;
    }
}
