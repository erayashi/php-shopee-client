<?php
/**
 * Created by eaz.
 * Date: 08/11/18
 * Time: 13:11
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\shop\request;


use ErryAz\ShopeeWrap\models\BaseRequest;
use ErryAz\ShopeeWrap\models\shop\ShopTrait;

class UpdateShopInfoRequest extends BaseRequest
{
    use ShopTrait;
}
