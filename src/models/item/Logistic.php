<?php
/**
 * Created by eaz.
 * Date: 03/11/18
 * Time: 12:17
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item;


use ErryAz\ShopeeWrap\models\BaseFormater;

class Logistic extends BaseFormater
{
    /** @var int */
    public $logistic_id;
    /** @var bool */
    public $enabled;
    /** @var float */
    public $shipping_fee;
    /** @var int */
    public $size_id;
    /** @var bool */
    public $is_free;

    /**
     * @return int
     */
    public function getLogisticId(): int
    {
        return $this->logistic_id;
    }

    /**
     * @param int $logistic_id
     */
    public function setLogisticId(int $logistic_id)
    {
        $this->logistic_id = $logistic_id;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     */
    public function setEnabled(bool $enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * @return float
     */
    public function getShippingFee(): float
    {
        return $this->shipping_fee;
    }

    /**
     * @param float $shipping_fee
     */
    public function setShippingFee(float $shipping_fee)
    {
        $this->shipping_fee = $shipping_fee;
    }

    /**
     * @return int
     */
    public function getSizeId(): int
    {
        return $this->size_id;
    }

    /**
     * @param int $size_id
     */
    public function setSizeId(int $size_id)
    {
        $this->size_id = $size_id;
    }

    /**
     * @return bool
     */
    public function isFree(): bool
    {
        return $this->is_free;
    }

    /**
     * @param bool $is_free
     */
    public function setIsFree(bool $is_free)
    {
        $this->is_free = $is_free;
    }
}
