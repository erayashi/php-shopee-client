<?php
/**
 * Created by eaz.
 * Date: 06/11/18
 * Time: 15:33
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item;


use ErryAz\ShopeeWrap\models\BaseFormater;

class Item extends BaseFormater
{
    use ItemTrait;
    use ItemIdTrait;

    /** @var Attributes */
    public $attributes;

    /**
     * @param Attributes $attributes
     */
    public function setAttributes(Attributes $attributes)
    {
        $this->attributes = $attributes;
    }
}
