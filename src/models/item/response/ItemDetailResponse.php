<?php
/**
 * Created by eaz.
 * Date: 30/10/18
 * Time: 10:33
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item\response;


use ErryAz\ShopeeWrap\models\BaseResponse;

class ItemDetailResponse extends BaseResponse
{
    /** @var ItemR */
    public $item;
    /** @var string */
    public $warning;

    /**
     * @param ItemR $item
     */
    public function setItem(ItemR $item)
    {
        $this->item = $item;
    }

    /**
     * @param string $warning
     */
    public function setWarning(string $warning)
    {
        $this->warning = $warning;
    }
}
