<?php
/**
 * Created by eaz.
 * Date: 07/02/19
 * Time: 10:09
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item\response;


use ErryAz\ShopeeWrap\models\BaseFormaterCollection;

class AttributesR extends BaseFormaterCollection
{

    /**
     * @param array $reqParam
     * @return $this
     * @throws \ReflectionException
     */
    public function fromArray(array $reqParam)
    {
        foreach ($reqParam as $param){
            $this->add(new AttributeR($param));
        }

        return $this;
    }
}
