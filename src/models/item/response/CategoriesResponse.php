<?php
/**
 * Created by eaz.
 * Date: 30/10/18
 * Time: 10:57
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item\response;


use ErryAz\ShopeeWrap\models\BaseResponse;
use ErryAz\ShopeeWrap\models\item\Categories;

class CategoriesResponse extends BaseResponse
{
    /** @var Categories */
    public $categories;

    /**
     * @param Categories $categories
     */
    public function setCategories(Categories $categories)
    {
        $this->categories = $categories;
    }
}
