<?php
/**
 * Created by eaz.
 * Date: 06/11/18
 * Time: 9:35
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item\response;


use ErryAz\ShopeeWrap\models\BaseResponse;
use ErryAz\ShopeeWrap\models\item\Item;
use ErryAz\ShopeeWrap\models\item\ItemIdTrait;

class AddItemResponse extends BaseResponse
{
    use ItemIdTrait;
    /** @var ItemR */
    public $item;
    /** @var string */
    public $warning;
    /** @var array */
    public $fail_image;

    /**
     * @param ItemR $item
     */
    public function setItem(ItemR $item)
    {
        $this->item = $item;
    }

    /**
     * @param string $warning
     */
    public function setWarning(string $warning)
    {
        $this->warning = $warning;
    }

    /**
     * @param array $fail_image
     */
    public function setFailImage(array $fail_image)
    {
        $this->fail_image = $fail_image;
    }
}
