<?php
/**
 * Created by eaz.
 * Date: 07/02/19
 * Time: 10:11
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item\response;


use ErryAz\ShopeeWrap\models\BaseFormater;
use ErryAz\ShopeeWrap\models\item\ItemIdTrait;
use ErryAz\ShopeeWrap\models\item\ItemTrait;

class ItemR extends BaseFormater
{
    /** @var AttributesR */
    public $attributes;

    use ItemIdTrait;
    use ItemTrait;

    /**
     * @param AttributesR $attributes
     */
    public function setAttributes(AttributesR $attributes)
    {
        $this->attributes = $attributes;
    }
}
