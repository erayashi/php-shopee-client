<?php
/**
 * Created by eaz.
 * Date: 31/10/18
 * Time: 17:49
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item\response;


use ErryAz\ShopeeWrap\models\BaseResponse;

class AttributesResponse extends BaseResponse
{
    /** @var AttributesR */
    public $attributes;

    /**
     * @param AttributesR $attributes
     */
    public function setAttributes(AttributesR $attributes)
    {
        $this->attributes = $attributes;
    }
}
