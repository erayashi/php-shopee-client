<?php
/**
 * Created by eaz.
 * Date: 30/10/18
 * Time: 10:35
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item\response;


use ErryAz\ShopeeWrap\models\BaseResponse;
use ErryAz\ShopeeWrap\models\item\Items;

class ItemListResponse extends BaseResponse
{
    /** @var Items */
    public $items;
    /** @var boolean */
    public $more;

    /**
     * @param Items $items
     */
    public function setItems(Items $items)
    {
        $this->items = $items;
    }

    /**
     * @param bool $more
     */
    public function setMore(bool $more)
    {
        $this->more = $more;
    }
}
