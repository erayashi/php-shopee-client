<?php
/**
 * Created by eaz.
 * Date: 07/02/19
 * Time: 10:06
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item\response;


use ErryAz\ShopeeWrap\models\item\Attribute;

class AttributeR extends Attribute
{
    /** @var int */
    public $attribute_id;
    /** @var string */
    public $attribute_name;
    /** @var bool */
    public $is_mandatory;
    /** @var string */
    public $attribute_type;
    /** @var string */
    public $attribute_value;
    /** @var string */
    public $input_type;
    /** @var array */
    public $options;

    /**
     * @param int $attribute_id
     */
    public function setAttributeId(int $attribute_id)
    {
        $this->attribute_id = $attribute_id;
    }

    /**
     * @param string $attribute_name
     */
    public function setAttributeName(string $attribute_name)
    {
        $this->attribute_name = $attribute_name;
    }

    /**
     * @param bool $is_mandatory
     */
    public function setIsMandatory(bool $is_mandatory)
    {
        $this->is_mandatory = $is_mandatory;
    }

    /**
     * @param string $attribute_type
     */
    public function setAttributeType(string $attribute_type)
    {
        $this->attribute_type = $attribute_type;
    }

    /**
     * @param string $attribute_value
     */
    public function setAttributeValue(string $attribute_value)
    {
        $this->attribute_value = $attribute_value;
    }

    /**
     * @param string $input_type
     */
    public function setInputType(string $input_type)
    {
        $this->input_type = $input_type;
    }

    /**
     * @param array $options
     */
    public function setOptions(array $options)
    {
        $this->options = $options;
    }
}
