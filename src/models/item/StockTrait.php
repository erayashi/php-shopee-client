<?php
/**
 * Created by eaz.
 * Date: 04/11/18
 * Time: 18:46
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item;


trait StockTrait
{
    /** @var integer */
    public $stock;

    /**
     * @return int
     */
    public function getStock(): int
    {
        return $this->stock;
    }

    /**
     * @param int $stock
     */
    public function setStock(int $stock)
    {
        $this->stock = $stock;
    }

}
