<?php
/**
 * Created by eaz.
 * Date: 11/12/18
 * Time: 9:50
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item;


trait TierVariationTrait
{
    /** @var TierVariations */
    public $tier_variation;

    /**
     * @return TierVariations
     */
    public function getTierVariation(): TierVariations
    {
        return $this->tier_variation;
    }

    /**
     * @param TierVariations $tier_variation
     */
    public function setTierVariation(TierVariations $tier_variation)
    {
        $this->tier_variation = $tier_variation;
    }

}
