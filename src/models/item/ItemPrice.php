<?php
/**
 * Created by eaz.
 * Date: 04/12/18
 * Time: 15:17
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item;


use ErryAz\ShopeeWrap\models\BaseFormater;

class ItemPrice extends BaseFormater
{
    use ItemIdTrait, PriceTrait;
}
