<?php
/**
 * Created by eaz.
 * Date: 30/10/18
 * Time: 10:52
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item;


use ErryAz\ShopeeWrap\models\BaseFormater;

class Category extends BaseFormater
{
    /** @var integer */
    public $parent_id;
    /** @var boolean */
    public $has_children;
    /** @var integer */
    public $category_id;
    /** @var string */
    public $category_name;

    /**
     * @return int
     */
    public function getParentId(): int
    {
        return $this->parent_id;
    }

    /**
     * @param int $parent_id
     */
    public function setParentId(int $parent_id)
    {
        $this->parent_id = $parent_id;
    }

    /**
     * @return bool
     */
    public function isHasChildren(): bool
    {
        return $this->has_children;
    }

    /**
     * @param bool $has_children
     */
    public function setHasChildren(bool $has_children)
    {
        $this->has_children = $has_children;
    }

    /**
     * @return int
     */
    public function getCategoryId(): int
    {
        return $this->category_id;
    }

    /**
     * @param int $category_id
     */
    public function setCategoryId(int $category_id)
    {
        $this->category_id = $category_id;
    }

    /**
     * @return string
     */
    public function getCategoryName(): string
    {
        return $this->category_name;
    }

    /**
     * @param string $category_name
     */
    public function setCategoryName(string $category_name)
    {
        $this->category_name = $category_name;
    }


}
