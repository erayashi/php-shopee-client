<?php
/**
 * Created by eaz.
 * Date: 03/11/18
 * Time: 12:21
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item;

use ErryAz\ShopeeWrap\models\BaseFormater;

class Wholesale extends BaseFormater
{
    /** @var int */
    public $min;
    /** @var int */
    public $max;
    /** @var float */
    public $unit_price;

    /**
     * @return int
     */
    public function getMin(): int
    {
        return $this->min;
    }

    /**
     * @param int $min
     */
    public function setMin(int $min)
    {
        $this->min = $min;
    }

    /**
     * @return int
     */
    public function getMax(): int
    {
        return $this->max;
    }

    /**
     * @param int $max
     */
    public function setMax(int $max)
    {
        $this->max = $max;
    }

    /**
     * @return float
     */
    public function getUnitPrice(): float
    {
        return $this->unit_price;
    }

    /**
     * @param float $unit_price
     */
    public function setUnitPrice(float $unit_price)
    {
        $this->unit_price = $unit_price;
    }
}
