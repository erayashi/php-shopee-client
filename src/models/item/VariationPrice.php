<?php
/**
 * Created by eaz.
 * Date: 04/12/18
 * Time: 15:20
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item;


class VariationPrice extends ItemPrice
{
    use VariationIdTrait;
}
