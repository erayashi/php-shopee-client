<?php
/**
 * Created by eaz.
 * Date: 04/11/18
 * Time: 18:52
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item;


trait VariationIdTrait
{
    /** @var int */
    public $variation_id;

    /**
     * @return mixed
     */
    public function getVariationId() : int
    {
        return $this->variation_id;
    }

    /**
     * @param mixed $variation_id
     */
    public function setVariationId(int $variation_id)
    {
        $this->variation_id = $variation_id;
    }

}
