<?php
/**
 * Created by eaz.
 * Date: 11/12/18
 * Time: 9:19
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item;


use ErryAz\ShopeeWrap\models\BaseFormater;

class VariationTier extends BaseFormater
{
    use PriceTrait, StockTrait;
    /** @var int[] */
    public $tier_index;
    /** @var string */
    public $variation_sku;

    /**
     * @return int[]
     */
    public function getTierIndex(): array
    {
        return $this->tier_index;
    }

    /**
     * @param int[] $tier_index
     */
    public function setTierIndex(array $tier_index)
    {
        $this->tier_index = $tier_index;
    }

    /**
     * @return string
     */
    public function getVariationSku(): string
    {
        return $this->variation_sku;
    }

    /**
     * @param string $variation_sku
     */
    public function setVariationSku(string $variation_sku)
    {
        $this->variation_sku = $variation_sku;
    }
}
