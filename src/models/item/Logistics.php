<?php
/**
 * Created by eaz.
 * Date: 04/11/18
 * Time: 14:08
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item;


use ErryAz\ShopeeWrap\models\BaseFormaterCollection;

class Logistics extends BaseFormaterCollection
{

    /**
     * @param array $reqParam
     * @return $this
     * @throws \ReflectionException
     */
    public function fromArray(array $reqParam)
    {
        foreach ($reqParam as $param){
            $this->add(new Logistic($param));
        }

        return $this;
    }
}
