<?php
/**
 * Created by eaz.
 * Date: 29/10/18
 * Time: 17:05
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item;

use ErryAz\ShopeeWrap\models\Images;

trait ItemTrait
{
    use PriceTrait;
    use StockTrait;
    /** @var string */
    public $item_sku;
    /** @var string */
    public $status;
    /** @var string */
    public $name;
    /** @var string */
    public $description;

    /**
     * @return Images
     */
    public function getImages(): Images
    {
        return $this->images;
    }

    /**
     * @param Images $images
     */
    public function setImages(Images $images)
    {
        $this->images = $images;
    }
    /** @var Images */
    public $images;
    /** @var string */
    public $currency;
    /** @var boolean */
    public $has_variation;
    /** @var float */
    public $weight;
    /** @var integer */
    public $category_id;
    /** @var float */
    public $original_price;
    /** @var float */
    public $rating_star;
    /** @var integer */
    public $cmt_count;
    /** @var integer */
    public $sales;
    /** @var integer */
    public $views;
    /** @var integer */
    public $likes;
    /** @var integer */
    public $package_length;
    /** @var integer */
    public $package_width;
    /** @var integer */
    public $package_height;
    /** @var integer */
    public $days_to_ship;
    /** @var string */
    public $size_chart;
    /** @var string */
    public $condition;
    /** @var integer */
    public $discount_id;
    /** @var integer */
    public $create_time;
    /** @var integer */
    public $update_time;
    /** @var Variations */
    public $variations;
    /** @var Logistics */
    public $logistics;
    /** @var Wholesales */
    public $wholesales;

    /**
     * @return string
     */
    public function getItemSku(): string
    {
        return $this->item_sku;
    }

    /**
     * @param string $item_sku
     */
    public function setItemSku(string $item_sku)
    {
        $this->item_sku = $item_sku;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency(string $currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return bool
     */
    public function isHasVariation(): bool
    {
        return $this->has_variation;
    }

    /**
     * @param bool $has_variation
     */
    public function setHasVariation(bool $has_variation)
    {
        $this->has_variation = $has_variation;
    }

    /**
     * @return float
     */
    public function getWeight(): float
    {
        return $this->weight;
    }

    /**
     * @param float $weight
     */
    public function setWeight(float $weight)
    {
        $this->weight = $weight;
    }

    /**
     * @return int
     */
    public function getCategoryId(): int
    {
        return $this->category_id;
    }

    /**
     * @param int $category_id
     */
    public function setCategoryId(int $category_id)
    {
        $this->category_id = $category_id;
    }

    /**
     * @return float
     */
    public function getOriginalPrice(): float
    {
        return $this->original_price;
    }

    /**
     * @param float $original_price
     */
    public function setOriginalPrice(float $original_price)
    {
        $this->original_price = $original_price;
    }

    /**
     * @return float
     */
    public function getRatingStar(): float
    {
        return $this->rating_star;
    }

    /**
     * @param float $rating_star
     */
    public function setRatingStar(float $rating_star)
    {
        $this->rating_star = $rating_star;
    }

    /**
     * @return int
     */
    public function getCmtCount(): int
    {
        return $this->cmt_count;
    }

    /**
     * @param int $cmt_count
     */
    public function setCmtCount(int $cmt_count)
    {
        $this->cmt_count = $cmt_count;
    }

    /**
     * @return int
     */
    public function getSales(): int
    {
        return $this->sales;
    }

    /**
     * @param int $sales
     */
    public function setSales(int $sales)
    {
        $this->sales = $sales;
    }

    /**
     * @return int
     */
    public function getViews(): int
    {
        return $this->views;
    }

    /**
     * @param int $views
     */
    public function setViews(int $views)
    {
        $this->views = $views;
    }

    /**
     * @return int
     */
    public function getLikes(): int
    {
        return $this->likes;
    }

    /**
     * @param int $likes
     */
    public function setLikes(int $likes)
    {
        $this->likes = $likes;
    }

    /**
     * @return int
     */
    public function getPackageLength(): int
    {
        return $this->package_length;
    }

    /**
     * @param int $package_length
     */
    public function setPackageLength(int $package_length)
    {
        $this->package_length = $package_length;
    }

    /**
     * @return int
     */
    public function getPackageWidth(): int
    {
        return $this->package_width;
    }

    /**
     * @param int $package_width
     */
    public function setPackageWidth(int $package_width)
    {
        $this->package_width = $package_width;
    }

    /**
     * @return int
     */
    public function getPackageHeight(): int
    {
        return $this->package_height;
    }

    /**
     * @param int $package_height
     */
    public function setPackageHeight(int $package_height)
    {
        $this->package_height = $package_height;
    }

    /**
     * @return int
     */
    public function getDaysToShip(): int
    {
        return $this->days_to_ship;
    }

    /**
     * @param int $days_to_ship
     */
    public function setDaysToShip(int $days_to_ship)
    {
        $this->days_to_ship = $days_to_ship;
    }

    /**
     * @return string
     */
    public function getSizeChart(): string
    {
        return $this->size_chart;
    }

    /**
     * @param string $size_chart
     */
    public function setSizeChart(string $size_chart)
    {
        $this->size_chart = $size_chart;
    }

    /**
     * @return string
     */
    public function getCondition(): string
    {
        return $this->condition;
    }

    /**
     * @param string $condition
     */
    public function setCondition(string $condition)
    {
        $this->condition = $condition;
    }

    /**
     * @return int
     */
    public function getDiscountId(): int
    {
        return $this->discount_id;
    }

    /**
     * @param int $discount_id
     */
    public function setDiscountId(int $discount_id)
    {
        $this->discount_id = $discount_id;
    }

    /**
     * @return int
     */
    public function getCreateTime(): int
    {
        return $this->create_time;
    }

    /**
     * @param int $create_time
     */
    public function setCreateTime(int $create_time)
    {
        $this->create_time = $create_time;
    }

    /**
     * @return int
     */
    public function getUpdateTime(): int
    {
        return $this->update_time;
    }

    /**
     * @param int $update_time
     */
    public function setUpdateTime(int $update_time)
    {
        $this->update_time = $update_time;
    }

    /**
     * @return Variations
     */
    public function getVariations(): Variations
    {
        return $this->variations;
    }

    /**
     * @param Variations $variations
     */
    public function setVariations(Variations $variations)
    {
        $this->variations = $variations;
    }

    /**
     * @return Logistics
     */
    public function getLogistics(): Logistics
    {
        return $this->logistics;
    }

    /**
     * @param Logistics $logistics
     */
    public function setLogistics(Logistics $logistics)
    {
        $this->logistics = $logistics;
    }

    /**
     * @return Wholesales
     */
    public function getWholesales(): Wholesales
    {
        return $this->wholesales;
    }

    /**
     * @param Wholesales $wholesales
     */
    public function setWholesales(Wholesales $wholesales)
    {
        $this->wholesales = $wholesales;
    }
}
