<?php
/**
 * Created by eaz.
 * Date: 02/11/18
 * Time: 17:12
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item;


trait ItemIdTrait
{
    /** @var integer */
    public $item_id;

    /**
     * @return int
     */
    public function getItemId(): int
    {
        return $this->item_id;
    }

    /**
     * @param int $item_id
     */
    public function setItemId(int $item_id)
    {
        $this->item_id = $item_id;
    }

}
