<?php
/**
 * Created by eaz.
 * Date: 11/12/18
 * Time: 10:01
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item;


use ErryAz\ShopeeWrap\models\BaseFormater;

class VariationIndex extends BaseFormater
{
    /** @var int[] */
    public $tier_index;
    /** @var int[] */
    public $variation_id;

    /**
     * @return int[]
     */
    public function getTierIndex(): array
    {
        return $this->tier_index;
    }

    /**
     * @param int[] $tier_index
     */
    public function setTierIndex(array $tier_index)
    {
        $this->tier_index = $tier_index;
    }

    /**
     * @return int[]
     */
    public function getVariationId(): array
    {
        return $this->variation_id;
    }

    /**
     * @param int[] $variation_id
     */
    public function setVariationId(array $variation_id)
    {
        $this->variation_id = $variation_id;
    }
}
