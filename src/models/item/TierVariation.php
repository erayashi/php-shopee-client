<?php
/**
 * Created by eaz.
 * Date: 11/12/18
 * Time: 9:17
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item;


use ErryAz\ShopeeWrap\models\BaseFormater;

class TierVariation extends BaseFormater
{
    /** @var string */
    public $name;
    /** @var string[] */
    public $options;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string[]
     */
    public function getOptions(): array
    {
        return $this->options;
    }

    /**
     * @param string[] $options
     */
    public function setOptions(array $options)
    {
        $this->options = $options;
    }
}
