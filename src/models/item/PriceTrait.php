<?php
/**
 * Created by eaz.
 * Date: 04/11/18
 * Time: 18:47
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item;


trait PriceTrait
{
    /** @var float */
    public $price;

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price)
    {
        $this->price = $price;
    }

}
