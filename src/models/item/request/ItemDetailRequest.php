<?php
/**
 * Created by eaz.
 * Date: 01/11/18
 * Time: 14:28
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item\request;


use ErryAz\ShopeeWrap\models\BaseRequest;
use ErryAz\ShopeeWrap\models\item\ItemIdTrait;

class ItemDetailRequest extends BaseRequest
{
    use ItemIdTrait;
}
