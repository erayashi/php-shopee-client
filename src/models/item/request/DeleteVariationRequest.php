<?php
/**
 * Created by eaz.
 * Date: 11/12/18
 * Time: 10:14
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item\request;


use ErryAz\ShopeeWrap\models\BaseRequest;
use ErryAz\ShopeeWrap\models\item\ItemIdTrait;
use ErryAz\ShopeeWrap\models\item\VariationIdTrait;

class DeleteVariationRequest extends BaseRequest
{
    use ItemIdTrait, VariationIdTrait;
}
