<?php
/**
 * Created by eaz.
 * Date: 11/12/18
 * Time: 9:42
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item\request;


use ErryAz\ShopeeWrap\models\BaseRequest;
use ErryAz\ShopeeWrap\models\item\ItemIdTrait;
use ErryAz\ShopeeWrap\models\item\VariationTiers;

class AddTierVariationRequest extends BaseRequest
{
    use ItemIdTrait;
    /** @var VariationTiers */
    public $variation;

    /**
     * @return VariationTiers
     */
    public function getVariation(): VariationTiers
    {
        return $this->variation;
    }

    /**
     * @param VariationTiers $variation
     */
    public function setVariation(VariationTiers $variation)
    {
        $this->variation = $variation;
    }
}
