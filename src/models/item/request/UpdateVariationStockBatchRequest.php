<?php
/**
 * Created by eaz.
 * Date: 04/12/18
 * Time: 18:34
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item\request;


use ErryAz\ShopeeWrap\models\BaseRequest;
use ErryAz\ShopeeWrap\models\item\VariationStocks;

class UpdateVariationStockBatchRequest extends BaseRequest
{
    /** @var VariationStocks */
    public $variations;

    /**
     * @return VariationStocks
     */
    public function getVariations(): VariationStocks
    {
        return $this->variations;
    }

    /**
     * @param VariationStocks $variations
     */
    public function setVariations(VariationStocks $variations)
    {
        $this->variations = $variations;
    }
}
