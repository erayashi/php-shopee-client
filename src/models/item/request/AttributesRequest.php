<?php
/**
 * Created by eaz.
 * Date: 30/10/18
 * Time: 14:19
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item\request;


use ErryAz\ShopeeWrap\models\BaseRequest;

class AttributesRequest extends BaseRequest
{
    /** @var integer */
    public $category_id;
    /** @var string */
    public $country;
    /** @var boolean */
    public $is_cb;
    /** @var string */
    public $language;

    /**
     * @return int
     */
    public function getCategoryId(): int
    {
        return $this->category_id;
    }

    /**
     * @param int $category_id
     */
    public function setCategoryId(int $category_id)
    {
        $this->category_id = $category_id;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry(string $country)
    {
        $this->country = $country;
    }

    /**
     * @return bool
     */
    public function isCb(): bool
    {
        return $this->is_cb;
    }

    /**
     * @param bool $is_cb
     */
    public function setIsCb(bool $is_cb)
    {
        $this->is_cb = $is_cb;
    }

    /**
     * @return string
     */
    public function getLanguage(): string
    {
        return $this->language;
    }

    /**
     * @param string $language
     */
    public function setLanguage(string $language)
    {
        $this->language = $language;
    }

}
