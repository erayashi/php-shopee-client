<?php
/**
 * Created by eaz.
 * Date: 11/12/18
 * Time: 9:58
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item\request;


use ErryAz\ShopeeWrap\models\BaseRequest;
use ErryAz\ShopeeWrap\models\item\ItemIdTrait;
use ErryAz\ShopeeWrap\models\item\VariationIndex;

class UpdateTierVariationIndexRequest extends BaseRequest
{
    use ItemIdTrait;
    /** @var VariationIndex */
    public $variation;

    /**
     * @return VariationIndex
     */
    public function getVariation(): VariationIndex
    {
        return $this->variation;
    }

    /**
     * @param VariationIndex $variation
     */
    public function setVariation(VariationIndex $variation)
    {
        $this->variation = $variation;
    }
}
