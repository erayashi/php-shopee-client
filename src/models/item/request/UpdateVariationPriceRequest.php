<?php
/**
 * Created by eaz.
 * Date: 04/11/18
 * Time: 18:54
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item\request;


use ErryAz\ShopeeWrap\models\BaseRequest;
use ErryAz\ShopeeWrap\models\item\ItemIdTrait;
use ErryAz\ShopeeWrap\models\item\PriceTrait;
use ErryAz\ShopeeWrap\models\item\VariationIdTrait;

class UpdateVariationPriceRequest extends BaseRequest
{
    use ItemIdTrait;
    use VariationIdTrait;
    use PriceTrait;
}
