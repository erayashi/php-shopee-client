<?php
/**
 * Created by eaz.
 * Date: 04/11/18
 * Time: 18:35
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item\request;


use ErryAz\ShopeeWrap\models\BaseRequest;
use ErryAz\ShopeeWrap\models\item\ItemIdTrait;
use ErryAz\ShopeeWrap\models\item\StockTrait;

class UpdateItemStockRequest extends BaseRequest
{
    use ItemIdTrait;
    use StockTrait;
}
