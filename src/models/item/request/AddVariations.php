<?php
/**
 * Created by eaz.
 * Date: 11/12/18
 * Time: 10:12
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item\request;


use ErryAz\ShopeeWrap\models\BaseRequest;
use ErryAz\ShopeeWrap\models\item\ItemIdTrait;
use ErryAz\ShopeeWrap\models\item\Variations;

class AddVariations extends BaseRequest
{
    use ItemIdTrait;

    /** @var Variations */
    public $variations;

    /**
     * @return Variations
     */
    public function getVariations(): Variations
    {
        return $this->variations;
    }

    /**
     * @param Variations $variations
     */
    public function setVariations(Variations $variations)
    {
        $this->variations = $variations;
    }
}
