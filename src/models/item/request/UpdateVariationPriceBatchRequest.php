<?php
/**
 * Created by eaz.
 * Date: 04/12/18
 * Time: 18:33
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item\request;


use ErryAz\ShopeeWrap\models\BaseRequest;
use ErryAz\ShopeeWrap\models\item\VariationPrices;

class UpdateVariationPriceBatchRequest extends BaseRequest
{
    /** @var VariationPrices */
    public $variations;

    /**
     * @return VariationPrices
     */
    public function getVariations(): VariationPrices
    {
        return $this->variations;
    }

    /**
     * @param VariationPrices $variations
     */
    public function setVariations(VariationPrices $variations)
    {
        $this->variations = $variations;
    }
}
