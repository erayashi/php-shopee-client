<?php
/**
 * Created by eaz.
 * Date: 30/10/18
 * Time: 10:50
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item\request;

use ErryAz\ShopeeWrap\models\BaseRequest;

class CategoriesCountryRequest extends BaseRequest
{
    /** @var string */
    public $country;
    /** @var int */
    public $is_cb;

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry(string $country)
    {
        $this->country = $country;
    }

    /**
     * @return int
     */
    public function getisCb(): int
    {
        return $this->is_cb;
    }

    /**
     * @param int $is_cb
     */
    public function setIsCb(int $is_cb)
    {
        $this->is_cb = $is_cb;
    }
}
