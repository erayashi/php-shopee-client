<?php
/**
 * Created by eaz.
 * Date: 04/11/18
 * Time: 18:16
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item\request;

use ErryAz\ShopeeWrap\models\BaseRequest;
use ErryAz\ShopeeWrap\models\item\ItemIdTrait;
use ErryAz\ShopeeWrap\models\item\ItemTrait;

class UpdateItemRequest extends BaseRequest
{
    use ItemIdTrait;
    use ItemTrait;
}
