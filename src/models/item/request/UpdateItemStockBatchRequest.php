<?php
/**
 * Created by eaz.
 * Date: 04/12/18
 * Time: 18:32
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item\request;


use ErryAz\ShopeeWrap\models\BaseRequest;
use ErryAz\ShopeeWrap\models\item\ItemStocks;

class UpdateItemStockBatchRequest extends BaseRequest
{
    /** @var ItemStocks */
    public $items;

    /**
     * @return ItemStocks
     */
    public function getItems(): ItemStocks
    {
        return $this->items;
    }

    /**
     * @param ItemStocks $items
     */
    public function setItems(ItemStocks $items)
    {
        $this->items = $items;
    }
}
