<?php
/**
 * Created by eaz.
 * Date: 11/12/18
 * Time: 10:20
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item\request;


use ErryAz\ShopeeWrap\models\BaseRequest;
use ErryAz\ShopeeWrap\models\item\ItemIdTrait;

class InsertItemImage extends BaseRequest
{
    use ItemIdTrait;
    /** @var string */
    public $image_url;
    /** @var int */
    public $image_position;

    /**
     * @return string
     */
    public function getImageUrl(): string
    {
        return $this->image_url;
    }

    /**
     * @param string $image_url
     */
    public function setImageUrl(string $image_url)
    {
        $this->image_url = $image_url;
    }

    /**
     * @return int
     */
    public function getImagePosition(): int
    {
        return $this->image_position;
    }

    /**
     * @param int $image_position
     */
    public function setImagePosition(int $image_position)
    {
        $this->image_position = $image_position;
    }
}
