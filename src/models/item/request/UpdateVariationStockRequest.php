<?php
/**
 * Created by eaz.
 * Date: 04/11/18
 * Time: 18:55
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item\request;


use ErryAz\ShopeeWrap\models\BaseRequest;
use ErryAz\ShopeeWrap\models\item\ItemIdTrait;
use ErryAz\ShopeeWrap\models\item\StockTrait;
use ErryAz\ShopeeWrap\models\item\VariationIdTrait;

class UpdateVariationStockRequest extends BaseRequest
{
    use ItemIdTrait;
    use VariationIdTrait;
    use StockTrait;
}
