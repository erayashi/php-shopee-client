<?php
/**
 * Created by eaz.
 * Date: 04/12/18
 * Time: 18:31
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item\request;


use ErryAz\ShopeeWrap\models\BaseRequest;
use ErryAz\ShopeeWrap\models\item\ItemPrices;

class UpdateItemPriceBatchRequest extends BaseRequest
{
    /** @var ItemPrices */
    public $items;

    /**
     * @return ItemPrices
     */
    public function getItems(): ItemPrices
    {
        return $this->items;
    }

    /**
     * @param ItemPrices $items
     */
    public function setItems(ItemPrices $items)
    {
        $this->items = $items;
    }
}
