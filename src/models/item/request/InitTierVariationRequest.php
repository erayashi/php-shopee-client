<?php
/**
 * Created by eaz.
 * Date: 10/12/18
 * Time: 16:59
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item\request;

use ErryAz\ShopeeWrap\models\item\VariationTiers;

class InitTierVariationRequest extends UpdateTierVariationListRequest
{
    /** @var VariationTiers */
    public $variation;

    /**
     * @return VariationTiers
     */
    public function getVariation(): VariationTiers
    {
        return $this->variation;
    }

    /**
     * @param VariationTiers $variation
     */
    public function setVariation(VariationTiers $variation)
    {
        $this->variation = $variation;
    }
}
