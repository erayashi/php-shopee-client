<?php
/**
 * Created by eaz.
 * Date: 11/12/18
 * Time: 10:18
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item\request;


class DeleteItemImages extends AddItemImages
{
    /** @var int[] */
    public $positions;

    /**
     * @return int[]
     */
    public function getPositions(): array
    {
        return $this->positions;
    }

    /**
     * @param int[] $positions
     */
    public function setPositions(array $positions)
    {
        $this->positions = $positions;
    }
}
