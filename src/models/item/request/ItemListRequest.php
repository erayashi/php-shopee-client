<?php
/**
 * Created by eaz.
 * Date: 01/11/18
 * Time: 14:12
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item\request;


use ErryAz\ShopeeWrap\models\BaseRequest;
use ErryAz\ShopeeWrap\models\PagingTrait;

class ItemListRequest extends BaseRequest
{
   use PagingTrait;
}
