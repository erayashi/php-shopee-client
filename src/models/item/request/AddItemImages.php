<?php
/**
 * Created by eaz.
 * Date: 11/12/18
 * Time: 10:16
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item\request;


use ErryAz\ShopeeWrap\models\BaseRequest;
use ErryAz\ShopeeWrap\models\item\ItemIdTrait;

class AddItemImages extends BaseRequest
{
    use ItemIdTrait;
    /** @var string[] */
    public $images;

    /**
     * @return string[]
     */
    public function getImages(): array
    {
        return $this->images;
    }

    /**
     * @param string[] $images
     */
    public function setImages(array $images)
    {
        $this->images = $images;
    }
}
