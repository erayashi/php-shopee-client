<?php
/**
 * Created by eaz.
 * Date: 04/11/18
 * Time: 14:05
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item\request;


use ErryAz\ShopeeWrap\models\BaseRequest;
use ErryAz\ShopeeWrap\models\item\Attributes;
use ErryAz\ShopeeWrap\models\item\ItemTrait;

class AddItemRequest extends BaseRequest
{
    use ItemTrait;
    /** @var Attributes */
    public $attributes;

    /**
     * @param Attributes $attributes
     */
    public function setAttributes(Attributes $attributes)
    {
        $this->attributes = $attributes;
    }
}
