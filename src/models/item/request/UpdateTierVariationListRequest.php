<?php
/**
 * Created by eaz.
 * Date: 11/12/18
 * Time: 9:52
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item\request;


use ErryAz\ShopeeWrap\models\BaseRequest;
use ErryAz\ShopeeWrap\models\item\ItemIdTrait;
use ErryAz\ShopeeWrap\models\item\TierVariationTrait;

class UpdateTierVariationListRequest extends BaseRequest
{
    use ItemIdTrait, TierVariationTrait;
}
