<?php
/**
 * Created by eaz.
 * Date: 11/12/18
 * Time: 9:18
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item;


use ErryAz\ShopeeWrap\models\BaseFormaterCollection;

class TierVariations extends BaseFormaterCollection
{
    /**
     * @param array $reqParam
     * @return $this
     * @throws \ReflectionException
     */
    public function fromArray(array $reqParam)
    {
        foreach ($reqParam as $param) {
            $this->add(new TierVariation($param));
        }
        return $this;
    }
}
