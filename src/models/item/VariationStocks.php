<?php
/**
 * Created by eaz.
 * Date: 04/12/18
 * Time: 15:15
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item;


use ErryAz\ShopeeWrap\models\BaseFormaterCollection;

class VariationStocks extends BaseFormaterCollection
{

    /**
     * @param array $reqParam
     * @return $this
     * @throws \ReflectionException
     */
    public function fromArray(array $reqParam)
    {
        foreach ($reqParam as $param) {
            $this->add(new VariationStock($param));
        }

        return $this;
    }
}
