<?php
/**
 * Created by eaz.
 * Date: 03/11/18
 * Time: 11:25
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item;


use ErryAz\ShopeeWrap\models\BaseFormater;

class Variation extends BaseFormater
{
    const mandatory_fields = ['name', 'variation_sku'];

    use VariationIdTrait;
    /** @var string */
    public $name;
    /** @var int */
    public $stock;
    /** @var float */
    public $price;
    /** @var string */
    public $variation_sku;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getStock(): int
    {
        return $this->stock;
    }

    /**
     * @param int $stock
     */
    public function setStock(int $stock)
    {
        $this->stock = $stock;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price)
    {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getVariationSku(): string
    {
        return $this->variation_sku;
    }

    /**
     * @param string $variation_sku
     */
    public function setVariationSku(string $variation_sku)
    {
        $this->variation_sku = $variation_sku;
    }
}
