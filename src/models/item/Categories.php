<?php
/**
 * Created by eaz.
 * Date: 05/02/19
 * Time: 21:06
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item;


use ErryAz\ShopeeWrap\models\BaseFormaterCollection;

class Categories extends BaseFormaterCollection
{

    public function fromArray(array $reqParam)
    {
        foreach ($reqParam as $param){
            $this->add(new Category($param));
        }

        return $this;
    }
}
