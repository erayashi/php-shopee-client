<?php
/**
 * Created by eaz.
 * Date: 04/12/18
 * Time: 15:14
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item;


class VariationStock extends ItemStock
{
    use VariationIdTrait;
}
