<?php
/**
 * Created by eaz.
 * Date: 03/11/18
 * Time: 11:34
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models\item;

use ErryAz\ShopeeWrap\models\BaseFormater;

class Attribute extends BaseFormater
{
    /** @var int */
    public $attributes_id;
    /** @var string */
    public $value;

    /**
     * @return int
     */
    public function getAttributesId(): int
    {
        return $this->attributes_id;
    }

    /**
     * @param int $attributes_id
     */
    public function setAttributesId(int $attributes_id)
    {
        $this->attributes_id = $attributes_id;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue(string $value)
    {
        $this->value = $value;
    }
}
