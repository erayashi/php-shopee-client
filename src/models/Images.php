<?php
/**
 * Created by eaz.
 * Date: 14/12/18
 * Time: 15:22
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models;


class Images extends BaseFormaterCollection
{

    public function fromArray(array $reqParam)
    {
        foreach ($reqParam as $param){
            $this->add(new Image($param));
        }

        return $this;
    }
}
