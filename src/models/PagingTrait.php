<?php
/**
 * Created by eaz.
 * Date: 02/11/18
 * Time: 18:56
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models;


trait PagingTrait
{
    /** @var integer */
    public $pagination_offset;
    /** @var integer */
    public $pagination_entries_per_page;
    /** @var integer */
    public $update_time_from;
    /** @var integer */
    public $update_time_to;

    public function setPaging(int $pagination_offset = 0,
                              int $pagination_entries_per_page = 10, int $update_time_from = 0,
                              int $update_time_to = 0){
        $this->pagination_offset = $pagination_offset;
        $this->pagination_entries_per_page = $pagination_entries_per_page;
        $this->update_time_from = $update_time_from;
        $this->update_time_to = $update_time_to;
    }

    /**
     * @return int
     */
    public function getPaginationOffset(): int
    {
        return $this->pagination_offset;
    }

    /**
     * @param int $pagination_offset
     */
    public function setPaginationOffset(int $pagination_offset)
    {
        $this->pagination_offset = $pagination_offset;
    }

    /**
     * @return int
     */
    public function getPaginationEntriesPerPage(): int
    {
        return $this->pagination_entries_per_page;
    }

    /**
     * @param int $pagination_entries_per_page
     */
    public function setPaginationEntriesPerPage(int $pagination_entries_per_page)
    {
        $this->pagination_entries_per_page = $pagination_entries_per_page;
    }

    /**
     * @return int
     */
    public function getUpdateTimeFrom(): int
    {
        return $this->update_time_from;
    }

    /**
     * @param int $update_time_from
     */
    public function setUpdateTimeFrom(int $update_time_from)
    {
        $this->update_time_from = $update_time_from;
    }

    /**
     * @return int
     */
    public function getUpdateTimeTo(): int
    {
        return $this->update_time_to;
    }

    /**
     * @param int $update_time_to
     */
    public function setUpdateTimeTo(int $update_time_to)
    {
        $this->update_time_to = $update_time_to;
    }
}
