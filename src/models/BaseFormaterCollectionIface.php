<?php
/**
 * Created by eaz.
 * Date: 04/12/18
 * Time: 11:42
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\models;


interface BaseFormaterCollectionIface
{
    public function fromArray(array $reqParam);
    public function toRequestData();
}
