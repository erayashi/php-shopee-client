<?php
/**
 * Created by eaz.
 * Date: 31/10/18
 * Time: 11:12
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\handler\exception;


use Exception;
use Throwable;

class ShopeeResException extends Exception
{
    protected $type = "";

    public function __construct($message, $type, int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }
}
