<?php
/**
 * Created by eaz.
 * Date: 01/11/18
 * Time: 11:03
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\handler;


use ErryAz\ShopeeWrap\handler\exception\ShopeeResException;
use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\ResponseInterface;

class ResponseHandler
{
    /**
     * @param ResponseInterface $response
     * @return mixed
     * @throws ShopeeResException
     */
    public static function success(ResponseInterface $response){
        $decodedBody = json_decode($response->getBody());
        if($response->getStatusCode() >= 300 || isset($decodedBody->error)) {
            self::throwShopeeError($response);
        }
        return $decodedBody;
    }

    /**
     * @param $e
     * @throws ShopeeResException
     */
    public static function error($e){
        if($e instanceof RequestException){
            self::throwShopeeError($e->getResponse());
        } else {
            throw new ShopeeResException("UNKNOWN ERROR", "OTHER", 500);
        }
    }

    /**
     * @param ResponseInterface $response
     * @throws ShopeeResException
     */
    public static function throwShopeeError($response){
        if($response instanceof ResponseInterface) {
            $error = json_decode($response->getBody());
            if(empty($error)) {
                throw new ShopeeResException("UNKNOWN ERROR", "OTHER", 500);
            }
            $error->msg = isset($error->msg) ? $error->msg : "SERVER_ERROR";
            throw new ShopeeResException($error->msg, $error->error, $response->getStatusCode());
        } else {
            throw new ShopeeResException("UNKNOWN ERROR", "OTHER", 500);
        }
    }
}
