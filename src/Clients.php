<?php
/**
 * Created by eaz.
 * Date: 30/10/18
 * Time: 13:28
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap;


use ErryAz\ShopeeWrap\api\ItemApi;
use ErryAz\ShopeeWrap\api\LogisticApi;
use ErryAz\ShopeeWrap\api\OrderApi;
use ErryAz\ShopeeWrap\api\ShopApi;

class Clients
{
    public $shopee;

    public function __construct(array $config)
    {
        $this->shopee = new Shopee($config);
    }

    public function setShopId(int $shopId){
        $this->shopee->shop_id = $shopId;
    }

    /**
     * @param string $path
     * @param null $body
     * @param bool $async
     * @return mixed
     * @throws handler\exception\ShopeeResException
     */
    public function send(string $path, $body = null, bool $async = false){
        return $this->shopee->send($path,$body,$async);
    }

    /**
     * @return ItemApi
     */
    public function item() : ItemApi {
        return new ItemApi($this);
    }

    /**
     * @return LogisticApi
     */
    public function logistic() : LogisticApi {
        return new LogisticApi($this);
    }

    /**
     * @return OrderApi
     */
    public function order() : OrderApi {
        return new OrderApi($this);
    }

    /**
     * @return ShopApi
     */
    public function shop() : ShopApi {
        return new ShopApi($this);
    }
}
