<?php
/**
 * Created by eaz.
 * Date: 04/11/18
 * Time: 19:03
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\api;


use ErryAz\ShopeeWrap\Clients;
use ErryAz\ShopeeWrap\Shopee;

class BaseApi
{
    /** @var Shopee  */
    protected $shopee;
    /** @var Clients  */
    protected $client;

    public function __construct(Clients $client)
    {
        $this->client = $client;
        $this->shopee = $client->shopee;
    }
}
