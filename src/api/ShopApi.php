<?php
/**
 * Created by eaz.
 * Date: 08/11/18
 * Time: 13:07
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\api;


use ErryAz\ShopeeWrap\models\BaseRequest;
use ErryAz\ShopeeWrap\models\shop\request\UpdateShopInfoRequest;
use ErryAz\ShopeeWrap\models\shop\response\ShopInfoResponse;
use Prophecy\Promise\PromiseInterface;

class ShopApi extends BaseApi
{
    const GET_INFO = "/shop/get";
    const UPDATE_INFO = "/shop/update";
    const GET_PERFORMANCE = "/shop/performance";

    /**
     * @param BaseRequest $request
     * @param bool $async
     * @return ShopInfoResponse|PromiseInterface
     * @throws \ErryAz\ShopeeWrap\handler\exception\ShopeeResException
     */
    public function getInfo(BaseRequest $request, bool $async = false){
        return $this->shopee->send(self::GET_INFO, $request, $async, ShopInfoResponse::class);
    }

    /**
     * @param UpdateShopInfoRequest $request
     * @param bool $async
     * @return ShopInfoResponse|PromiseInterface
     * @throws \ErryAz\ShopeeWrap\handler\exception\ShopeeResException
     */
    public function updateInfo(UpdateShopInfoRequest $request, bool $async = false){
        return $this->shopee->send(self::UPDATE_INFO, $request, $async, ShopInfoResponse::class);
    }

    /**
     * @param BaseRequest $request
     * @param bool $async
     * @return PromiseInterface|mixed
     * @throws \ErryAz\ShopeeWrap\handler\exception\ShopeeResException
     */
    public function getPerformance(BaseRequest $request, bool $async = false){
        return $this->shopee->send(self::GET_PERFORMANCE, $request, $async);
    }
}
