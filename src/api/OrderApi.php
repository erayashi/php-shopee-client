<?php
/**
 * Created by eaz.
 * Date: 04/11/18
 * Time: 19:40
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\api;


use ErryAz\ShopeeWrap\models\BaseFormaterIface;
use ErryAz\ShopeeWrap\models\order\request\OrderCancelRequest;
use ErryAz\ShopeeWrap\models\order\request\OrderDetailsRequest;
use ErryAz\ShopeeWrap\models\order\response\OrderDetailResponse;
use ErryAz\ShopeeWrap\models\order\response\OrderListResponse;

class OrderApi extends BaseApi
{
    const ADD_NOTE              = "/orders/note/add";
    const CANCEL                = "/orders/cancel";
    const ESCROW_DETAIL_GET     = "/orders/my_income";
    const DETAILS_GET           = "/orders/detail";
    const GET_BY_STATUS         = "/orders/get";
    const LIST_GET              = "/orders/basics";
    const ACCEPT_CANCELLATION   = "/orders/buyer_cancellation/accept";
    const REJECT_CANCELLATION   = "/orders/buyer_cancellation/reject";

    /**
     * @param BaseFormaterIface $request
     * @param bool $async
     * @return mixed
     * @throws \ErryAz\ShopeeWrap\handler\exception\ShopeeResException
     */
    public function addNote(BaseFormaterIface $request, bool $async = false){
        return $this->shopee->send(self::ADD_NOTE, $request, $async);
    }

    /**
     * @param OrderCancelRequest $request
     * @param bool $async
     * @return mixed
     * @throws \ErryAz\ShopeeWrap\handler\exception\ShopeeResException
     */
    public function cancel(OrderCancelRequest $request, bool $async = false){
        return $this->shopee->send(self::CANCEL, $request, $async);
    }

    /**
     * @param BaseFormaterIface $request
     * @param bool $async
     * @return mixed
     * @throws \ErryAz\ShopeeWrap\handler\exception\ShopeeResException
     */
    public function getEscrowDetails(BaseFormaterIface $request, bool $async = false){
        return $this->shopee->send(self::ESCROW_DETAIL_GET, $request, $async);
    }

    /**
     * @param OrderDetailsRequest $request
     * @param bool $async
     * @return OrderDetailResponse
     * @throws \ErryAz\ShopeeWrap\handler\exception\ShopeeResException
     */
    public function getDetails(OrderDetailsRequest $request, bool $async = false){
        return $this->shopee->send(self::DETAILS_GET, $request, $async,
            OrderDetailResponse::class);
    }

    /**
     * @param BaseFormaterIface $request
     * @param bool $async
     * @return OrderListResponse
     * @throws \ErryAz\ShopeeWrap\handler\exception\ShopeeResException
     */
    public function getByStatus(BaseFormaterIface $request, bool $async = false){
        return $this->shopee->send(self::GET_BY_STATUS, $request, $async,
            OrderListResponse::class);
    }

    /**
     * @param BaseFormaterIface $request
     * @param bool $async
     * @return OrderListResponse
     * @throws \ErryAz\ShopeeWrap\handler\exception\ShopeeResException
     */
    public function getList(BaseFormaterIface $request, bool $async = false){
        return $this->shopee->send(self::LIST_GET, $request, $async,
            OrderListResponse::class);
    }

    /**
     * @param BaseFormaterIface $request
     * @param bool $async
     * @return mixed
     * @throws \ErryAz\ShopeeWrap\handler\exception\ShopeeResException
     */
    public function acceptBuyerCancellation(BaseFormaterIface $request, bool $async = false){
        return $this->shopee->send(self::ACCEPT_CANCELLATION, $request, $async);
    }

    /**
     * @param BaseFormaterIface $request
     * @param bool $async
     * @return mixed
     * @throws \ErryAz\ShopeeWrap\handler\exception\ShopeeResException
     */
    public function rejectBuyerCancellation(BaseFormaterIface $request, bool $async = false){
        return $this->shopee->send(self::REJECT_CANCELLATION, $request, $async);
    }
}
