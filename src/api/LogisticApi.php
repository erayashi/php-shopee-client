<?php
/**
 * Created by eaz.
 * Date: 04/11/18
 * Time: 19:44
 * Github: https://github.com/erry-az
 */

namespace ErryAz\ShopeeWrap\api;


use ErryAz\ShopeeWrap\models\BaseFormaterIface;
use ErryAz\ShopeeWrap\models\logistic\request\InitRequest;
use ErryAz\ShopeeWrap\models\logistic\request\ParameterForInitRequest;
use ErryAz\ShopeeWrap\models\logistic\request\TimeSlotRequest;
use ErryAz\ShopeeWrap\models\logistic\response\InitResponse;
use ErryAz\ShopeeWrap\models\logistic\response\LogisticListResponse;

class LogisticApi extends BaseApi
{
    const LIST_GET = "/logistics/channel/get";
    const INIT_PARAM_GET = "/logistics/init_parameter/get";
    const ADDRESS_GET = "/logistics/address/get";
    const TIME_SLOT_GET = "/logistics/timeslot/get";
    const BRANCH_GET = "/logistics/branch/get";
    const INFO_GET = "/logistics/init_info/get";
    const INIT = "/logistics/init";
    const AIRWAY_BILL_GET = "/logistics/airway_bill/get_mass";
    const ORDER_LOGISTICS_GET = "/logistics/order/get";
    const LOGISTICS_MESSAGE_GET = "/logistics/tracking";

    /**
     * @param BaseFormaterIface $request
     * @param bool $async
     * @return LogisticListResponse
     * @throws \ErryAz\ShopeeWrap\handler\exception\ShopeeResException
     */
    public function getList(BaseFormaterIface $request, bool $async = false){
        return $this->shopee->send(self::LIST_GET, $request, $async);
    }

    /**
     * @param ParameterForInitRequest $request
     * @param bool $async
     * @return mixed
     * @throws \ErryAz\ShopeeWrap\handler\exception\ShopeeResException
     */
    public function getInitParameter(ParameterForInitRequest $request, bool $async = false){
        return $this->shopee->send(self::INIT_PARAM_GET, $request, $async);
    }

    /**
     * @param BaseFormaterIface $request
     * @param bool $async
     * @return mixed
     * @throws \ErryAz\ShopeeWrap\handler\exception\ShopeeResException
     */
    public function getAddress(BaseFormaterIface $request, bool $async = false){
        return $this->shopee->send(self::ADDRESS_GET, $request, $async);
    }

    /**
     * @param TimeSlotRequest $request
     * @param bool $async
     * @return mixed
     * @throws \ErryAz\ShopeeWrap\handler\exception\ShopeeResException
     */
    public function getTimeSlot(TimeSlotRequest $request, bool $async = false){
        return $this->shopee->send(self::TIME_SLOT_GET, $request, $async);
    }

    /**
     * @param BaseFormaterIface $request
     * @param bool $async
     * @return mixed
     * @throws \ErryAz\ShopeeWrap\handler\exception\ShopeeResException
     */
    public function getBranch(BaseFormaterIface $request, bool $async = false){
        return $this->shopee->send(self::BRANCH_GET, $request, $async);
    }

    /**
     * @param BaseFormaterIface $request
     * @param bool $async
     * @return mixed
     * @throws \ErryAz\ShopeeWrap\handler\exception\ShopeeResException
     */
    public function getInfo(BaseFormaterIface $request, bool $async = false){
        return $this->shopee->send(self::INFO_GET, $request, $async);
    }

    /**
     * @param InitRequest $request
     * @param bool $async
     * @return mixed | InitResponse
     * @throws \ErryAz\ShopeeWrap\handler\exception\ShopeeResException
     */
    public function init(InitRequest $request, bool $async = false){
        return $this->shopee->send(self::INIT, $request, $async, InitResponse::class);
    }

    /**
     * @param BaseFormaterIface $request
     * @param bool $async
     * @return mixed
     * @throws \ErryAz\ShopeeWrap\handler\exception\ShopeeResException
     */
    public function getAirwayBill(BaseFormaterIface $request, bool $async = false){
        return $this->shopee->send(self::AIRWAY_BILL_GET, $request, $async);
    }

    /**
     * @param BaseFormaterIface $request
     * @param bool $async
     * @return mixed
     * @throws \ErryAz\ShopeeWrap\handler\exception\ShopeeResException
     */
    public function getOrderLogistics(BaseFormaterIface $request, bool $async = false){
        return $this->shopee->send(self::ORDER_LOGISTICS_GET, $request, $async);
    }

    /**
     * @param BaseFormaterIface $request
     * @param bool $async
     * @return mixed
     * @throws \ErryAz\ShopeeWrap\handler\exception\ShopeeResException
     */
    public function getLogisticsMessage(BaseFormaterIface $request, bool $async = false){
        return $this->shopee->send(self::LOGISTICS_MESSAGE_GET, $request, $async);
    }
}
