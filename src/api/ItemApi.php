<?php
/*
 * This file is part of <package name>.
 *
 * This source file is subject to the license that is bundled
 * with this source code in the file LICENSE.
 */

namespace ErryAz\ShopeeWrap\api;

use ErryAz\ShopeeWrap\handler\exception\ShopeeResException;
use ErryAz\ShopeeWrap\models\BaseRequest;
use ErryAz\ShopeeWrap\models\item\request\AddItemImages;
use ErryAz\ShopeeWrap\models\item\request\AddItemRequest;
use ErryAz\ShopeeWrap\models\item\request\AddTierVariationRequest;
use ErryAz\ShopeeWrap\models\item\request\AddVariations;
use ErryAz\ShopeeWrap\models\item\request\AttributesRequest;
use ErryAz\ShopeeWrap\models\item\request\CategoriesCountryRequest;
use ErryAz\ShopeeWrap\models\item\request\DeleteItemImages;
use ErryAz\ShopeeWrap\models\item\request\DeleteVariationRequest;
use ErryAz\ShopeeWrap\models\item\request\InitTierVariationRequest;
use ErryAz\ShopeeWrap\models\item\request\InsertItemImage;
use ErryAz\ShopeeWrap\models\item\request\ItemDetailRequest;
use ErryAz\ShopeeWrap\models\item\request\ItemListRequest;
use ErryAz\ShopeeWrap\models\item\request\UpdateItemPriceBatchRequest;
use ErryAz\ShopeeWrap\models\item\request\UpdateItemPriceRequest;
use ErryAz\ShopeeWrap\models\item\request\UpdateItemRequest;
use ErryAz\ShopeeWrap\models\item\request\UpdateItemStockBatchRequest;
use ErryAz\ShopeeWrap\models\item\request\UpdateItemStockRequest;
use ErryAz\ShopeeWrap\models\item\request\UpdateTierVariationIndexRequest;
use ErryAz\ShopeeWrap\models\item\request\UpdateTierVariationListRequest;
use ErryAz\ShopeeWrap\models\item\request\UpdateVariationPriceBatchRequest;
use ErryAz\ShopeeWrap\models\item\request\UpdateVariationPriceRequest;
use ErryAz\ShopeeWrap\models\item\request\UpdateVariationStockBatchRequest;
use ErryAz\ShopeeWrap\models\item\request\UpdateVariationStockRequest;
use ErryAz\ShopeeWrap\models\item\response\AddItemResponse;
use ErryAz\ShopeeWrap\models\item\response\AttributesResponse;
use ErryAz\ShopeeWrap\models\item\response\CategoriesResponse;
use ErryAz\ShopeeWrap\models\item\response\ItemDetailResponse;
use ErryAz\ShopeeWrap\models\item\response\ItemListResponse;
use GuzzleHttp\Promise\PromiseInterface;

class ItemApi extends BaseApi
{
    const CATEGORIES_GET_BY_COUNTRY = '/item/categories/get_by_country';
    const CATEGORIES_GET_BY_SHOPID = '/item/categories/get';
    const ATTRIBUTES_GET = '/item/attributes/get';
    const LIST_GET = '/items/get';
    const DETAIL_GET = '/item/get';
    const ADD = '/item/add';
    const UPDATE = '/item/update';
    const DELETE = '/item/delete';
    const VARIATION_ADD = '/item/add_variations';
    const IMAGES_ADD = '/item/img/add';
    const IMAGES_DELETE = '/item/img/delete';
    const VARIATION_DELETE = '/item/delete_variation';
    const IMAGES_INSERT = '/item/img/insert';
    const PRICE_UPDATE = '/items/update_price';
    const STOCK_UPDATE = '/items/update_stock';
    const VARIATION_PRICE_UPDATE = '/items/update_variation_price';
    const VARIATION_STOCK_UPDATE = '/items/update_variation_stock';
    const PRICE_UPDATES = '/items/update/items_price';
    const STOCK_UPDATES = '/items/update/items_stock';
    const VARIATION_PRICE_UPDATES = '/items/update/vars_price';
    const VARIATION_STOCK_UPDATES = '/items/update/vars_stock';
    const VARIATION_TIER_INIT = '/item/tier_var/init';
    const VARIATION_TIER_ADD = '/item/tier_var/add';
    const VARIATION_TIER_GET = '/item/tier_var/get';
    const VARIATION_TIER_LIST_UPDATE = '/item/tier_var/update_list';
    const VARIATION_TIER_INDEX_UPDATE = '/item/tier_var/update';

    /**
     * @param CategoriesCountryRequest  $request
     * @param bool $async
     *
     * @throws ShopeeResException
     *
     * @return CategoriesResponse|PromiseInterface
     */
    public function getCategoriesByCountry(CategoriesCountryRequest $request, bool $async = false)
    {
        return $this->shopee->send(self::CATEGORIES_GET_BY_COUNTRY, $request, $async,
            CategoriesResponse::class);
    }

    /**
     * @param BaseRequest $request
     * @param bool $async
     *
     * @throws ShopeeResException
     *
     * @return CategoriesResponse|PromiseInterface
     */
    public function getCategoriesByShopID(BaseRequest $request, bool $async = false)
    {
        return $this->shopee->send(self::CATEGORIES_GET_BY_SHOPID, $request, $async,
            CategoriesResponse::class);
    }

    /**
     * @param AttributesRequest $request
     * @param bool $async
     *
     * @throws ShopeeResException
     *
     * @return AttributesResponse|PromiseInterface
     */
    public function getAttributes(AttributesRequest $request, bool $async = false)
    {
        return $this->shopee->send(self::ATTRIBUTES_GET, $request, $async,
            AttributesResponse::class);
    }

    /**
     * @param ItemDetailRequest $request
     * @param bool $async
     *
     * @throws ShopeeResException
     *
     * @return mixed
     */
    public function getDetail(ItemDetailRequest $request, bool $async = false)
    {
        return $this->shopee->send(self::DETAIL_GET, $request, $async, ItemDetailResponse::class);
    }

    /**
     * @param ItemListRequest $request
     * @param bool $async
     *
     * @throws ShopeeResException
     *
     * @return ItemListResponse|PromiseInterface
     */
    public function getList(ItemListRequest $request, bool $async = false)
    {
        return $this->shopee->send(self::LIST_GET, $request, $async, ItemListResponse::class);
    }

    /**
     * @param AddItemRequest $request
     * @param bool $async
     *
     * @throws ShopeeResException
     *
     * @return AddItemResponse|PromiseInterface
     */
    public function add(AddItemRequest $request, bool $async = false)
    {
        return $this->shopee->send(self::ADD, $request, $async, AddItemResponse::class);
    }

    /**
     * @param UpdateItemRequest $request
     * @param bool $async
     *
     * @throws ShopeeResException
     *
     * @return AddItemResponse|PromiseInterface
     */
    public function update(UpdateItemRequest $request, bool $async = false)
    {
        return $this->shopee->send(self::UPDATE, $request, $async, AddItemResponse::class);
    }

    /**
     * @param ItemDetailRequest $request
     * @param bool $async
     *
     * @throws ShopeeResException
     *
     * @return mixed
     */
    public function delete(ItemDetailRequest $request, bool $async = false)
    {
        return $this->shopee->send(self::DELETE, $request, $async);
    }

    /**
     * @param AddVariations $request
     * @param bool $async
     *
     * @throws ShopeeResException
     *
     * @return mixed
     */
    public function addVariations(AddVariations $request, bool $async = false)
    {
        return $this->shopee->send(self::VARIATION_ADD, $request, $async);
    }

    /**
     * @param AddItemImages $request
     * @param bool $async
     *
     * @throws ShopeeResException
     *
     * @return mixed
     */
    public function addImages(AddItemImages $request, bool $async = false)
    {
        return $this->shopee->send(self::IMAGES_ADD, $request, $async);
    }

    /**
     * @param DeleteItemImages $request
     * @param bool $async
     *
     * @throws ShopeeResException
     *
     * @return mixed
     */
    public function deleteImages(DeleteItemImages $request, bool $async = false)
    {
        return $this->shopee->send(self::IMAGES_DELETE, $request, $async);
    }

    /**
     * @param DeleteVariationRequest $request
     * @param bool $async
     *
     * @throws ShopeeResException
     *
     * @return mixed
     */
    public function deleteVariation(DeleteVariationRequest $request, bool $async = false)
    {
        return $this->shopee->send(self::VARIATION_DELETE, $request, $async);
    }

    /**
     * @param InsertItemImage $request
     * @param bool $async
     *
     * @throws ShopeeResException
     *
     * @return mixed
     */
    public function insertImage(InsertItemImage $request, bool $async = false)
    {
        return $this->shopee->send(self::IMAGES_INSERT, $request, $async);
    }

    /**
     * @param UpdateItemPriceRequest $request
     * @param bool $async
     *
     * @throws ShopeeResException
     *
     * @return mixed
     */
    public function updatePrice(UpdateItemPriceRequest $request, bool $async = false)
    {
        return $this->shopee->send(self::PRICE_UPDATE, $request, $async);
    }

    /**
     * @param UpdateItemStockRequest $request
     * @param bool $async
     *
     * @throws ShopeeResException
     *
     * @return mixed
     */
    public function updateStock(UpdateItemStockRequest $request, bool $async = false)
    {
        return $this->shopee->send(self::STOCK_UPDATE, $request, $async);
    }

    /**
     * @param UpdateVariationPriceRequest $request
     * @param bool $async
     *
     * @throws ShopeeResException
     *
     * @return mixed
     */
    public function updateVariationPrice(UpdateVariationPriceRequest $request, bool $async = false)
    {
        return $this->shopee->send(self::VARIATION_PRICE_UPDATE, $request, $async);
    }

    /**
     * @param UpdateVariationStockRequest $request
     * @param bool $async
     *
     * @throws ShopeeResException
     *
     * @return mixed
     */
    public function updateVariationStock(UpdateVariationStockRequest $request, bool $async = false)
    {
        return $this->shopee->send(self::VARIATION_STOCK_UPDATE, $request, $async);
    }

    /**
     * @param UpdateItemPriceBatchRequest $request
     * @param bool $async
     *
     * @throws ShopeeResException
     *
     * @return mixed
     */
    public function updatePriceBatch(UpdateItemPriceBatchRequest $request, bool $async = false)
    {
        return $this->shopee->send(self::PRICE_UPDATES, $request, $async);
    }

    /**
     * @param UpdateItemStockBatchRequest $request
     * @param bool $async
     *
     * @throws ShopeeResException
     *
     * @return mixed
     */
    public function updateStockBatch(UpdateItemStockBatchRequest $request, bool $async = false)
    {
        return $this->shopee->send(self::STOCK_UPDATES, $request, $async);
    }

    /**
     * @param UpdateVariationPriceBatchRequest $request
     * @param bool $async
     *
     * @throws ShopeeResException
     *
     * @return mixed
     */
    public function updateVariationPriceBatch(UpdateVariationPriceBatchRequest $request, bool $async = false)
    {
        return $this->shopee->send(self::VARIATION_PRICE_UPDATES, $request, $async);
    }

    /**
     * @param UpdateVariationStockBatchRequest $request
     * @param bool $async
     *
     * @throws ShopeeResException
     *
     * @return mixed
     */
    public function updateVariationStockBatch(UpdateVariationStockBatchRequest $request, bool $async = false)
    {
        return $this->shopee->send(self::VARIATION_STOCK_UPDATES, $request, $async);
    }

    /**
     * @param InitTierVariationRequest $request
     * @param bool $async
     *
     * @throws ShopeeResException
     *
     * @return mixed
     */
    public function initTierVatiation(InitTierVariationRequest $request, bool $async = false)
    {
        return $this->shopee->send(self::VARIATION_TIER_INIT, $request, $async);
    }

    /**
     * @param AddTierVariationRequest $request
     * @param bool $async
     *
     * @throws ShopeeResException
     *
     * @return mixed
     */
    public function addTierVatiation(AddTierVariationRequest $request, bool $async = false)
    {
        return $this->shopee->send(self::VARIATION_TIER_ADD, $request, $async);
    }

    /**
     * @param ItemDetailRequest $request
     * @param bool $async
     *
     * @throws ShopeeResException
     *
     * @return mixed
     */
    public function getTierVatiation(ItemDetailRequest $request, bool $async = false)
    {
        return $this->shopee->send(self::VARIATION_TIER_GET, $request, $async);
    }

    /**
     * @param UpdateTierVariationListRequest $request
     * @param bool $async
     *
     * @throws ShopeeResException
     *
     * @return mixed
     */
    public function updateTierVatiationList(UpdateTierVariationListRequest $request, bool $async = false)
    {
        return $this->shopee->send(self::VARIATION_TIER_LIST_UPDATE, $request, $async);
    }

    /**
     * @param UpdateTierVariationIndexRequest $request
     * @param bool $async
     *
     * @throws ShopeeResException
     *
     * @return mixed
     */
    public function updateTierVatiationIndex(UpdateTierVariationIndexRequest $request, bool $async = false)
    {
        return $this->shopee->send(self::VARIATION_TIER_LIST_UPDATE, $request, $async);
    }
}
