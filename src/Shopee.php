<?php

/*
 * This file is part of <package name>.
 *
 * This source file is subject to the license that is bundled
 * with this source code in the file LICENSE.
 */

namespace ErryAz\ShopeeWrap;

use ErryAz\ShopeeWrap\handler\exception\ShopeeResException;
use ErryAz\ShopeeWrap\handler\ResponseHandler;
use ErryAz\ShopeeWrap\models\BaseRequest;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use PHPUnit\Runner\Exception;

class Shopee
{
    const conf = 'shopee';
    const REDIRECT_FORMAT = '%s/api/v1/shop/auth_partner?id=%s&token=%s&redirect=%s';
    const S_URL = 'https://partner.shopeemobile.com';
    const T_URL = 'https://partner.uat.shopeemobile.com';
    /**
     * shopee partner id.
     *
     * @var int
     */
    public $partner_id;

    /**
     * shopee partner key.
     *
     * @var string
     */
    public $partner_key;

    /**
     * shopee shop id.
     *
     * @var int
     */
    public $shop_id;

    /**
     * query params.
     *
     * @var array
     */
    private $query = [];

    /**
     * active url.
     *
     * @var string
     */
    private $url;

    /**
     * guzzle http client.
     *
     * @var object
     */
    private $client;

    /**
     * api version.
     *
     * @var string
     */
    private $v;

    /** @var string */
    private $url_request;
    /** @var array */
    public $body_request;

    /**
     * initiate shoppeApi.
     *
     * @param array $configs
     */
    public function __construct(array $configs = null)
    {
        $this->partner_id = isset($configs['partner_id']) ? $configs['partner_id'] : 0;
        $this->partner_key = isset($configs['partner_key']) ? $configs['partner_key'] : '';
        $this->shop_id = isset($configs['shop_id']) ? $configs['shop_id'] : 0;
        $this->url = isset($configs['production']) && true === $configs['production'] ? self::S_URL : self::T_URL;
        $this->v = isset($configs['version']) ? $configs['version'] : 'v1';

        $this->client = new Client();
    }

    /**
     * @param int $partner_id
     * @param string $key
     * @param int $shop_id
     *
     * @return Shopee
     */
    public function factory(int $partner_id, string $key, $shop_id = 0)
    {
        return new static(['partner_id' => $partner_id, 'partner_key' => $key, 'shop_id' => $shop_id]);
    }

    /**
     * @param string $url
     * @param string $algo
     *
     * @return string
     */
    public function redirect(string $url, $algo = 'sha256')
    {
        $hash_token = hash($algo, $this->partner_key.$url);
        $redirect_url = sprintf(self::REDIRECT_FORMAT, $this->url, $this->partner_id, $hash_token, $url);

        return $redirect_url;
    }

    /**
     * @param string $key
     * @param string $val
     */
    public function addQuery(string $key, string $val)
    {
        $this->query[$key] = $val;
    }

    /**
     * @param array $queries
     */
    public function addQueries(array $queries)
    {
        foreach ($queries as $key => $val) {
            $this->query[$key] = $val;
        }
    }

    /**
     * @param string $path
     * @param null $body
     * @param bool $async
     * @param string|null $responseFormater
     * @return \GuzzleHttp\Promise\PromiseInterface|mixed|null
     * @throws ShopeeResException
     */
    public function send(string $path, $body = null, bool $async = false, string $responseFormater = null)
    {
        try {
            $this->preparation($path, $body);
        } catch (\ReflectionException $e) {
            throw new ShopeeResException($e->getMessage(), 'REFLECTION', $e->getCode(), $e);
        }

        if ($async) {
            return $this->client->requestAsync('POST', $this->url_request, $this->body_request);
        } else {
            $decodedResponse = null;
            try {
                $request = $this->client->request('POST', $this->url_request, $this->body_request);
                $decodedResponse = ResponseHandler::success($request);
                if(!empty($responseFormater)) $decodedResponse = new $responseFormater($decodedResponse);
            } catch (Exception $e) {
                ResponseHandler::error($e);
            } catch (GuzzleException $e) {
                ResponseHandler::error($e);
            }

            return $decodedResponse;
        }
    }

    /**
     * @param string $path
     * @param BaseRequest|null $body
     */
    private function preparation(string $path, BaseRequest $body = null)
    {
        if ($body instanceof BaseRequest) {
            $body->shopid = !isset($body->shopid) ? $this->shop_id : $body->shopid;
            $body->partner_id = $this->partner_id;
            $body->timestamp = time();
        } else {
            $body = new BaseRequest();
            $body->setShopid($this->shop_id);
            $body->setPartnerId($this->partner_id);
            $body->setTimestamp(time());
        }

        $this->url_request = $this->url.'/api/'.$this->v.$path;
        $this->body_request = [
            'headers' => ['Authorization' => hash_hmac('sha256', $this->url_request.'|'.json_encode($body),
                $this->partner_key), 'user-agent' => 'hehe'],
            'query' => $this->query,
            'json' => $body,
            'timeout' => 60,
        ];
    }

    /**
     * @return string
     */
    public function getUrlRequest(): string
    {
        return $this->url_request;
    }

    /**
     * @return array
     */
    public function getBodyRequest(): array
    {
        return $this->body_request;
    }
}
